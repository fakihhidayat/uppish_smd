@extends('layouts.admin')

@section('title')
    Download
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div>
            @endif
      </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="row">
                <div class="col-lg-8"><h4 class="card-title">Download</h4></div>
                <div class="col-lg-4" align="right">
                  <a href="{{ route('download.create')}}">
                      <button type="button" class="btn btn-primary btn-fw">
                      <i class="mdi mdi-plus"></i> Upload File</button>
                  </a>
                </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped" id="datatable-product">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Path</th>
                        <th>Created</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                       @forelse($download as $d)
                       <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $d->title }}</td>
                            <td>{{ $d->file }}</td>
                            <td>{{ $d->created_at->diffForHumans() }}</td>
                            <td><div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" title="Ubah" class="btn btn-dark btn-sm"><a href="{{ route('download.edit', $d->id) }}" style="color: white"><i class="mdi mdi-pencil"></i></a></button>
                                <form action="{{ route('download.destroy', $d->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" title="Hapus" class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                                </form>
                              </div>
                          </td>
                        </tr>
                       @empty
                        <tr class="bg-secondary">
                          <td colspan="6">Belum Ada Data</td>
                        </tr>
                       @endforelse
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

@endsection