
@extends('layouts.frontend')

@section('title')
SDM - {{ $hr->nama_sdm }}
@endsection

@section('content')
<div class="container">
    <br>
        <div class="section">
            <div class="row">
                @if(count($group) > 0)
                <div class="col-md-8 col-lg-9">
                    <div class="section world-news">
                        <h1 class="section-title">{{ $hr->nama_sdm }}</h1>	
                        <div class="world-nav cat-menu">         
                            <ul class="list-inline">                      
                                <li><a href="{{ route('humanresource.detail', $hr->id) }}" class="btn-show" title="{{ $hr->nama_sdm }}">Deskripsi Pekerjaan</a></li>
                            </ul> 					
                        </div>
                        <div class="post">
                            <div class="post-content">
                                <div class="entry-content">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="table-primary">
                                            <th scope="col">No</th>
                                            <th scope="col">Wilayah Kerja</th>
                                            <th scope="col">Nama Lengkap</th>
                                            <th scope="col">Desa/Kelurahan Dampingan</th>
                                            <th scope="col">Keterangan</th>
                                            <th scope="col">Detail</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                            <?php
                                            $no = 1;
                                            ?>
                                            @foreach ($group as $district => $employees)
                                            <?php
                                                $jml = count($employees);
                                            ?>
                                            <tr>
                                                <td rowspan="{{ $jml }}">{{ $no++ }}</td>
                                                <td rowspan="{{ $jml }}" align="center" class="bg-dark"><strong style="color: white">{{ $district }}</strong></td>
                                            @foreach ($employees as $e)
                                                <th>{{ $e->getEmployee->nama }}</th>
                                                <td>{{ $e->getVillage->nama_desa }}</td>
                                                <td>{{ $e->keterangan }}</td>
                                                <td><a href="{{ route('employee.detail', $e->nik_pegawai) }}" class="btn-show" title="Detail {{ $e->getEmployee->nama }}"><i class="fa fa-eye"></i></a></td>
                                            </tr>
                                            @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/post--> 
                    </div><!--/.section-->
                </div>
                @else
                <div class="col-lg-9" style="margin-top: -3px">
                    <div class="left-content">
                        <div class="details-news">											
                            <div class="post">
                                <div class="post-content">								
                                    <div class="entry-content" align="center">
                                        <h3>-- Belum Ada Data --</h3>
                                    </div>
                                </div>
                            </div><!--/post--> 
                        </div><!--/.section-->
                    </div><!--/.left-content-->
                </div>
                @endif
                @include('layouts.frontend_sidebar')
            </div>				
        </div><!--/.section-->
    </div><!--/.container-->
@endsection
