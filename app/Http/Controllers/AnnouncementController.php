<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Announcement;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announ = Announcement::orderBy('created_at', 'desc')->paginate(10);
        return view('announcement.index', compact('announ'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('announcement.formCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|string|max:255', 
            'deskripsi' => 'required'
        ]);

        $announ = new Announcement([
            'judul' => $request->get('judul'),
            'deskripsi' => $request->get('deskripsi'),
            'admin_id' => Auth::id()
        ]);
        $announ->save();
        return back()->with('success','Pengumuman Baru berhasil ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announ = Announcement::where('id', $id)->first();
        return view('announcement.formEdit', compact('announ')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|string|max:255', 
            'deskripsi' => 'required'
        ]);

        $announ = Announcement::find($id);
        $announ->judul = $request->get('judul');
        $announ->deskripsi = $request->get('deskripsi');
        $announ->admin_id = Auth::id();

        try {
            $announ->save();
            return redirect()->route('announcement.index')->with('success','Pengumuman berhasil diupdate !');
        }
        catch (\Exception $e) { 
            return back()->with('error','Pengumuman gagal diupdate !');
        }         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Announcement::destroy($id);
        return back()->with('success', 'Pengumuman berhasil dihapus!');
    }

    public function showById($id)
    {
        // Ambil Data Berita
        $ann = Announcement::find($id); 
        return view('announcement.showById', compact('ann'));
    }

    public function showAll()
    {
        // Ambil Semua Data Pengumuman
        $announcement = Announcement::orderBy('created_at', 'asc')->paginate(10); 

        return view('announcement.showAll', compact('announcement'));
    }
}
