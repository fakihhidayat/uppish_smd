@extends('layouts.admin')

@section('title')
    Post Pengumuman Baru
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>
        
            
        <div class="col-md-12 grid-margin stretch-card">
                
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Pengumuman | Post Pengumuman Baru</h4>
                <form class="forms-sample" action="{{ route('announcement.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Pengumuman" value="{{ old('judul') }}">
                        @if ($errors->has('judul'))
                        <small class="text-danger">{{ $errors->first('judul') }}</small>@endif
                    </div>
                    
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="form-control" id="deskripsi" rows="20" name="deskripsi">{{ old('deskripsi')}}</textarea>
                        @if ($errors->has('deskripsi'))
                        <small class="text-danger">{{ $errors->first('deskripsi') }}</small>@endif
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="{{ route('announcement.index') }}"><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
