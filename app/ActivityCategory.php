<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Activity;

class ActivityCategory extends Model
{
    public function getActivities()
    {
        return $this->hasMany(Activity::class, 'id_kategori', 'id');
    }
}
