
@extends('layouts.frontend')

@section('title')
Download
@endsection

@section('content')
<div class="container">
    <br>
        <div class="section">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div class="section world-news">
                        <h1 class="section-title">Daftar Download</h1>	
                        <div class="post">
                            <div class="post-content">
                                <div class="entry-content">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="table-primary">
                                            <th scope="col">No</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Download File</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                            <?php $no = 1; ?>
                                            @forelse ($download as $d)
                                            <tr>
                                                <td>{{ $no++ }}</th>
                                                <td>{{ $d->title }}</td>
                                                <td><a href="{{ Storage::url($d->file) }}">Download</a></td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="3">Belum Ada Data.</th>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/post--> 
                    </div><!--/.section-->
                </div>
                @include('layouts.frontend_sidebar')
            </div>				
        </div><!--/.section-->
    </div><!--/.container-->
@endsection
