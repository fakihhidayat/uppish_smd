<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Participant;
use App\ParticipantsCategory;
use App\SubDistrict;
use App\Village;
use DB;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peserta = ParticipantsCategory::orderBy('id', 'desc')->with(['getParticipant','getDistrict','getVillage'])->paginate(50);
        return view('participant.index', compact('peserta'));
    }

    public function showDataTahapI() {
        $app = SubDistrict::with('getParticipantKategori.getVillage')->get();
        // $hasil = [] ;
        // echo '<table border="1">
        //         <thead>
        //         <tr>
        //             <th>Kecamatan</th>
        //             <th>Desa</th>
        //             <th>Jumlah KPM</th>
        //             <th>Jumlah BUMIL</th>
        //             <th>Jumlah BALITA</th>
        //             <th>Jumlah APRAS</th>
        //             <th>Jumlah SD</th>
        //             <th>Jumlah SMP</th>
        //             <th>Jumlah SMA</th>
        //             <th>Jumlah LANSIA</th>
        //             <th>JUMLAH DISABELITAS</th>
        //         </tr>
        //         <thead>
        //         <tbody>';


        // foreach($app as $key => $v)
        // {
        //   //echo json_encode(array(3,2,1));

        //    echo '<tr>';
        //    echo '<td rowspan="'.($v->getParticipantKategori->count()!=0 || $v->getParticipantKategori->count()!=null ?($v->getParticipantKategori->count()+1):1).'">'.($v->nama_kecamatan).'</td>';
        //    echo '<td>';
        //    foreach($v->getParticipantKategori as $detail => $d)
        //    {
        //         echo '<tr>';
        //         echo '<td>'.$d->getVillage->nama_desa.'</td>';
        //         echo '<td>'.$d->jumlah.'</td>';
        //         echo '<td>'.$d->bumil.'</td>';
        //         echo '<td>'.$d->balita.'</td>';
        //         echo '<td>'.$d->apras.'</td>';
        //         echo '<td>'.$d->sd.'</td>';
        //         echo '<td>'.$d->smp.'</td>';
        //         echo '<td>'.$d->sma.'</td>';
        //         echo '<td>'.$d->lansia.'</td>';
        //         echo '<td>'.$d->disabilitas.'</td>';
        //         echo '</tr>';
        //    }
        //    echo '<tr>
        //         <td colspan="2"><b>'.$v->nama_kecamatan.' TOTAL</b></td>
        //         <td><b>'.($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('jumlah')->toArray())) : 0).'</b></td>
        //         <td>'.($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('bumil')->toArray())) : 0).'</td>
        //         <td>'.($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('balita')->toArray())) : 0).'</td>
        //         <td>'.($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('apras')->toArray())) : 0).'</td>
        //         <td>'.($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('sd')->toArray())) : 0).'</td>
        //         <td>'.($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('smp')->toArray())) : 0).'</td>
        //         <td>'.($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('sma')->toArray())) : 0).'</td>
        //         <td>'.($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('lansia')->toArray())) : 0).'</td>
        //         <td>'.($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('disabilitas')->toArray())) : 0).'</td>
        //      </tr>';

        // }
        // echo '</td></tr>';

        // exit;

        return view('participant.dataTahapI', compact('app'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = SubDistrict::orderBy('nama_kecamatan', 'asc')->pluck('nama_kecamatan', 'id');
        return view('participant.formCreate', compact('districts'));
    }

    public function getVillage($id)
    {
        $village = Village::where('id_subdistrict', $id)->pluck("nama_desa","id");
        return json_encode($village);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nopkh' => 'required|unique:participants,nomor_pkh',
            'nama' => 'required',
            'district' => 'required',
            'village' => 'required',
            'alamat' => 'required'
        ]);

        $nextId = DB::table('participants_categories')->max('id') + 1;
        $peserta = new Participant([
            'nomor_pkh' => $request->get('nopkh'),
            'nama' => $request->get('nama'),
            'alamat' => $request->get('alamat'),
            'id_detail' => $nextId
        ]);

        $detail = new ParticipantsCategory([
            'id_district' => $request->get('district'),
            'id_village' => $request->get('village'),
            'bumil' => $request->get('bumil'),
            'balita' => $request->get('balita'),
            'apras' => $request->get('apras'),
            'sd' => $request->get('sd'),
            'smp' => $request->get('smp'),
            'sma' => $request->get('sma'),
            'lansia' => $request->get('lansia'),
            'disabilitas' => $request->get('disabilitas'),
            'jumlah' => $request->get('total')
        ]);

        $peserta->save();
        $detail->save();
        return back()->with('success','Peserta PKH baru berhasil ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
