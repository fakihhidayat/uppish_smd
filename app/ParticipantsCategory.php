<?php

namespace App;
use App\Participant;

use Illuminate\Database\Eloquent\Model;

class ParticipantsCategory extends Model
{
    protected $fillable =
    [
        'id_district','bumil','balita','apras','sd','smp','sma','lansia','disabilitas','jumlah'
    ];

    public $timestamps = false;

    public function getParticipant()
    {
        return $this->hasOne(Participant::class, 'id_detail', 'id');
    }

    public function getVillage()
    {
        return $this->belongsTo(Village::Class, 'id_village', 'id');
    }

    public function getDistrict()
    {
        return $this->belongsTo(SubDistrict::Class, 'id_district', 'id');
    }
}
