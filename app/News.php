<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NewsCategory;
use App\NewsComment;
use App\User;

class News extends Model
{
    protected $fillable =
    [
        'id_kategori','judul','uraian','image'
    ];

    public function getCreatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])->format('d, M Y');
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    public function newsCategory()
    {
        return $this->belongsTo(NewsCategory::class, 'id_kategori', 'id');
    }

    public function getComment()
    {
        return $this->hasMany(NewsComment::class, 'id_berita', 'id');
    }

    public function getAdmin()
    {
        return $this->belongsTo(User::class, 'id_admin', 'id');
    }

}
