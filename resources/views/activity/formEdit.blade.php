@extends('layouts.admin')

@section('title')
    Update Kegiatan
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="flash-message">
                    <p class="alert alert-danger">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>

        <div class="col-md-12 grid-margin stretch-card">

            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Aktivitas | Update Kegiatan</h4>
                <form class="forms-sample" action="{{ route('activity.update', $act->id) }}" method="post" enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Info" value="{{ $act->judul }}">
                        @if ($errors->has('judul'))
                        <small class="text-danger">{{ $errors->first('judul') }}</small>@endif
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kategori">Kategori</label>
                                <select class="form-control" id="kategori" name="kategori">
                                    @if(count($kategori) > 0)
                                    @foreach($kategori as $kat)
                                        <option value="{{ $kat->id }}" {{ $kat->id == $act->activityCategory->id ? 'selected' : '' }}>{{ $kat->kat_kegiatan }}</option>
                                    @endforeach
                                    @else
                                    <option value="">-- Data Kategori Belum Tersedia --</option>
                                    @endif
                                </select>
                                @if ($errors->has('kategori'))
                                <small class="text-danger">{{ $errors->first('kategori') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Gambar/Foto</label>
                                <div class="input-group col-xs-12">
                                    <input type="file" name="image" id="profile-img" class="form-control file-upload-info" placeholder="Gambar/Foto Info">
                                </div>
                                @if ($errors->has('image'))
                                <small class="text-danger">{{ $errors->first('image') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-2">
                                <div class="form-group">
                                    <label>Preview</label><br>
                                    <img src="{{ asset('asset_general/images/activity/'.$act->foto.'') }}" id="profile-img-tag" width="150px" height="150px" />
                                </div>
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tgl">Tanggal</label>
                                <div class="input-group col-xs-12">
                                    <input type="text" name="tgl" id="tgl_kegiatan" class="form-control" placeholder="Tanggal Kegiatan" value="{{ $act->tgl_kegiatan }}">
                                </div>
                                @if ($errors->has('tgl'))
                                <small class="text-danger">{{ $errors->first('tgl') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lokasi">Lokasi</label>
                                <div class="input-group col-xs-12">
                                    <input type="text" name="lokasi" class="form-control" placeholder="Lokasi Kegiatan" value="{{ $act->lok_kegiatan }}">
                                </div>
                                @if ($errors->has('lokasi'))
                                <small class="text-danger">{{ $errors->first('lokasi') }}</small>@endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="uraian">Uraian</label>
                        <textarea class="form-control summernote-editor" id="uraian" rows="30" name="uraian">{{ $act->uraian }}</textarea>
                        @if ($errors->has('uraian'))
                        <small class="text-danger">{{ $errors->first('uraian') }}</small>@endif
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="{{ route('activity.index') }}"><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
</script>
@endpush
