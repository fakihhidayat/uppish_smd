<div class="row">
<div class="col-lg-3">
    <img src="{{ asset('asset_general/images/employee/'.$detail->foto.'') }}" width="110" height="150" postion="center">
</div>
<div class="col-lg-9">
<table class="table table-borderless">
    <tbody>
        <tr>
            <th>Tempat Lahir</th>
            <td>{{ $detail->tmp_lahir }}</td>
        </tr>
        <tr>
            <th>Tanggal Lahir</th>
            <td>{{ date('d, F Y', strtotime($detail->tgl_lahir)) }}</td>
        </tr>
        <tr>
            <th>Alamat</th>
            <td>{{ $detail->alamat }}</td>
        </tr>
        <tr>
            <th>E-mail</th>
            <td>{{ $detail->email }}</td>
        </tr>
        <tr>
            <th>Telp</th>
            <td>{{ $detail->telp }}</td>
        </tr>
    </tbody>
</table>
</div>
</div>