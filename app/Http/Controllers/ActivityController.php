<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\ActivityCategory;
use App\Activity;
use App\ActivityComment;
use App\Participant;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activity = Activity::with(['activityCategory','getComments'])->get();
        $group = $activity->groupBy(function ($activity){
            return $activity->activityCategory->kat_kegiatan;
        });
        return view('activity.index', compact('group'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Activity();
        $kategori = ActivityCategory::All()->sortBy('kat_kegiatan', SORT_NATURAL | SORT_FLAG_CASE)->pluck('kat_kegiatan', 'id');
        return view('activity.formCreate', compact(['model', 'kategori']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kategori' => 'required',
            'judul' => 'required|string|max:255|unique:news,judul',
            'tgl' => 'required',
            'lokasi' => 'required',
            'uraian' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $file       = Request()->file('image');
        $fileName   = $file->getClientOriginalName();
        Request()->file('image')->move(public_path('asset_general/images/activity'), $fileName);

        $act = new Activity([
            'id_kategori' => $request->get('kategori'),
            'judul' => $request->get('judul'),
            'tgl_kegiatan' => $request->get('tgl'),
            'lok_kegiatan' => $request->get('lokasi'),
            'foto' => $fileName,
            'uraian' => $request->get('uraian'),
            'admin_id' => Auth::id()
        ]);
        $act->save();
        return back()->with('success','Kegiatan Baru berhasil ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = ActivityCategory::find($id);
        $activities = Activity::where([
            ['status', 'published'],
            ['id_kategori', $id]
        ])->paginate(12);

        return view('activity.showByCategory', compact(['category','activities']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $act = Activity::where('id', $id)->with('activityCategory')->first();
        $kategori = ActivityCategory::orderBy('id', 'asc')->get();
        return view('activity.formEdit', compact(['act','kategori']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kategori' => 'required',
            'judul' => 'required|string|max:255',
            'tgl' => 'required',
            'lokasi' => 'required',
            'uraian' => 'required'
        ]);

        $act = Activity::find($id);
        $act->id_kategori = $request->get('kategori');
        $act->judul = $request->get('judul');
        $act->tgl_kegiatan = $request->get('tgl');
        $act->lok_kegiatan = $request->get('lokasi');
        $act->uraian = $request->get('uraian');
        $act->admin_id = Auth::id();

        $file = Request()->file('image');

        if($file) {
            $fileName   = $file->getClientOriginalName();
            Request()->file('image')->move(public_path('asset_general/images/activity'), $fileName);
            $act->foto = $fileName;
        }

        try {
            $act->save();
            return redirect()->route('activity.index')->with('success','Kegiatan berhasil diupdate !');
        }
        catch (\Exception $e) {
            return back()->with('error','Kegiatan gagal diupdate !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Activity::destroy($id);
        return back()->with('success', 'Kegiatan berhasil dihapus!');
        return redirect()->back();
    }

    public function showById($id)
    {
        // Ambil Data Kegiatan
        $activity = Activity::find($id);
        $idCategory = $activity->id_kategori;
        $category = ActivityCategory::find($idCategory);

        // Komentar
        $comments = ActivityComment::where('id_kegiatan', $id)->get();
        $publish = $comments->where('moderasi', 'yes');
        return view('activity.showById', compact(['category','activity','comments','publish']));
    }

    public function updateStatus(Request $request, $id)
    {
        $act = Activity::find($id);
        $act->status = $request->get('status');

        try {
            $act->save();
            return redirect()->route('activity.index')->with('success','Status Kegiatan berhasil diupdate !');
        }
        catch (\Exception $e) {
            return back()->with('error','Status Kegiatan gagal diupdate !');
        }
    }


    public function getDataChart()
    {
        $dataTemp = ['sed'=>[]] ;
        $dataTemp['labels'] = Participant::select(DB::raw('year(created_at) as year'))->groupBy('year')->pluck('year');
        $tahap1 = [] ;
        $tahap2 = [] ;
        $tahap3 = [] ;
        $tahap4 = [] ;
        foreach($dataTemp['labels'] as $d)
        {
            $dataTemp['datashets'][] = Participant::select('tahap',DB::raw('year(created_at) as year'),DB::raw('concat("Tahap ",tahap) as labels'))->where(DB::raw('year(created_at)'),$d)->orderBy('tahap','ASC')->get();
            // $k1 = 0 ;
            // $k2 = 0 ;
            // $k3 = 0 ;
            // $k4 = 0 ;

        }


        return \response()->json($dataTemp);
    }



}
