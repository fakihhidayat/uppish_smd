<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator,Redirect,Response;
use App\News;
use App\NewsCategory;
use App\NewsComment;
use App\Admin;
use DataTables;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('created_at', 'desc')->with(['newsCategory','getComment'])->paginate(10);
        // $admin = Auth::guard('admin');

        // dd($admin);
        return view('news.index', compact('news'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = NewsCategory::All()->sortBy('nama_kategori', SORT_NATURAL | SORT_FLAG_CASE)->pluck('nama_kategori', 'id');
        return view('news.formCreate', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kategori' => 'required',
            'judul' => 'required|string|max:255|unique:news,judul',
            'uraian' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $file       = Request()->file('image');
        $fileName   = $file->getClientOriginalName();
        Request()->file('image')->move(public_path('asset_general/images/news'), $fileName);

        $news = new News([
            'id_kategori' => $request->get('kategori'),
            'judul' => $request->get('judul'),
            'image' => $fileName,
            'uraian' => $request->get('uraian'),
            'admin_id' => Auth::id()
        ]);
        $news->save();
        return back()->with('success','Info Baru berhasil ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $category = NewsCategory::find($id);
        // $news = $category->news()->paginate(10);

        // return view('news.showByCategory', compact(['category','news']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::where('id', $id)->with('newsCategory')->first();
        $kategori = NewsCategory::orderBy('nama_kategori', 'asc')->get();
        return view('news.formEdit', compact(['news','kategori']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kategori' => 'required',
            'judul' => 'required|string|max:255',
            'uraian' => 'required'
        ]);

        $news = News::find($id);
        $news->id_kategori = $request->get('kategori');
        $news->judul = $request->get('judul');
        $news->uraian = $request->get('uraian');
        $news->admin_id = Auth::id();

        $file       = Request()->file('image');

        if($file) {
            $fileName   = $file->getClientOriginalName();
            Request()->file('image')->move(public_path('asset_general/images/news'), $fileName);
            $news->image = $fileName;
        }

        try {
            $news->save();
            return redirect()->route('news.index')->with('success','Info berhasil diupdate !');
        }
        catch (\Exception $e) {
            return back()->with('error','Info gagal diupdate !');
        }
    }

    public function updateStatus(Request $request, $id)
    {
        $news = News::find($id);
        $news->status = $request->get('status');

        try {
            $news->save();
            return redirect()->route('news.index')->with('success','Status Info berhasil diupdate !');
        }
        catch (\Exception $e) {
            return back()->with('error','Status Info gagal diupdate !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        News::destroy($id);
        return back()->with('success', 'Info berhasil dihapus!');
    }

    public function showById($id)
    {
        // Ambil Data Berita
        $news = News::find($id);
        $idCategory = $news->id_kategori;
        $category = NewsCategory::find($idCategory);

        // Komentar
        $comments = NewsComment::where('id_berita', $id)->get();
        $publish = $comments->where('moderasi', 'yes');
        return view('news.showById', compact(['category','news', 'comments','publish']));
    }

    public function showAll()
    {
        // Ambil Semua Data Berita
        $news = News::orderBy('created_at', 'desc')->where('status', 'published')->with(['newsCategory','getComment'])->paginate(12);

        return view('news.showAll', compact('news'));
    }

    public function storeCategory(Request $request)
    {
        Request()->validate([
            'kategori_new' => 'required'
        ]);

            $data = $request->all();
            $check = NewsCategory::insert($data);
            $arr = array('msg' => 'Something goes to wrong. Please try again lator', 'status' => false);
            if($check){
            $arr = array('msg' => 'Successfully submit form using ajax', 'status' => true);
            }
            return Response()->json($arr);
    }
}
