<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Contact;
use DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->paginate('20');

        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.formCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:255|unique:products,nama_produk',
            'harga' => 'required',
            'satuan' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $file       = Request()->file('image');
        $fileName   = $file->getClientOriginalName();
        Request()->file('image')->move(public_path('asset_general/images/product'), $fileName);
        $harga = explode(".",explode(" ",$request->get('harga'))[1]);
        $product = new Product([
            'nama_produk' => $request->get('nama'),
            'harga' => (int)implode($harga),
            'jual_per' => $request->get('satuan'),
            'foto' => $fileName,
            'deskripsi' => $request->get('deskripsi')
        ]);
        $product->save();
        return back()->with('success','Produk Baru berhasil ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::where('id', $id)->first();
        return view('product.formEdit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|string|max:255',
            'harga' => 'required',
            'satuan' => 'required'
        ]);

        $product = Product::find($id);
        $product->nama_produk = $request->get('nama');
        $product->harga = $request->get('harga');
        $product->jual_per = $request->get('satuan');
        $product->deskripsi = $request->get('deskripsi');


        $file       = Request()->file('image');

        if($file) {
            $fileName   = $file->getClientOriginalName();
            Request()->file('image')->move(public_path('asset_general/images/product'), $fileName);
            $product->foto = $fileName;
        }

        try {
            $product->save();
            return redirect()->route('product.index')->with('success','Produk berhasil diupdate !');
        }
        catch (\Exception $e) {
            return back()->with('error','Produk gagal diupdate !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        return back()->with('success', 'Produk berhasil dihapus!');
    }

    public function showAll()
    {
        // Ambil Semua Data Produk
        $products = Product::where('status', 'sale')->paginate('20');
        $contact  = Contact::All()->first();

        return view('product.showAll', compact(['products','contact']));
    }
}
