<?php

use Illuminate\Database\Seeder;

class DistrictVillageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('sub_districts')->insert(array(
        //     array('nama_kecamatan' => 'Cibugel', 'profil' => '', 'lat' => '-6.679', 'long' => '108.026', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 7, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Cimalaka', 'profil' => '', 'lat' => '-6.8025187', 'long' => '107.9413323', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 14, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Cimanggung', 'profil' => '', 'lat' => '-6.9500237', 'long' => '107.8462394', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 11, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Cisarua', 'profil' => '', 'lat' => '-6.8270449', 'long' => '107.9709613', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 7, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Cisitu', 'profil' => '', 'lat' => '-6.8390238', 'long' => '108.059866', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 10, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Conggeang', 'profil' => '', 'lat' => '-6.9500237', 'long' => '107.8465394', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 12, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Darmaraja', 'profil' => '', 'lat' => '-6.9211727', 'long' => '108.059866', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 16, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Ganeas', 'profil' => '', 'lat' => '-6.8632082', 'long' => '107.9650353', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 17, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Jatigede', 'profil' => '', 'lat' => '-6.8773203', 'long' => '108.1310083', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 12, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Jatinagor', 'profil' => '', 'lat' => '-6.9370992', 'long' => '107.7813872', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 12, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Jatinunggal', 'profil' => '', 'lat' => '-6.9370724', 'long' => '108.1784455', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 9, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Pamulihan', 'profil' => '', 'lat' => '', 'long' => '', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 11, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Paseh', 'profil' => '', 'lat' => '-6.7988053', 'long' => '108.0124469', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 10, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Rancakalong', 'profil' => '', 'lat' => '-6.8266568', 'long' => '107.8465394', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 10, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Situraja', 'profil' => '', 'lat' => '-6.8667448', 'long' => '108.0065201', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 14, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Sukasari', 'profil' => '', 'lat' => '-6.8704635', 'long' => '107.9775465', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 7, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Sumedang Utara', 'profil' => '', 'lat' => '-6.8293742', 'long' => '107.9116474', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 10, 'jml_kelurahan' => 3),
        //     array('nama_kecamatan' => 'Surian', 'profil' => '', 'lat' => '', 'long' => '', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 8, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Tanjungkerta', 'profil' => '', 'lat' => '-6.7418731', 'long' => '107.9176312', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 11, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Tanjungmedar', 'profil' => '', 'lat' => '-6.7231499', 'long' => '107.8702347', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 9, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Tanjungsari', 'profil' => '', 'lat' => '-6.8440915', 'long' => '107.7932319', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 12, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Tomo', 'profil' => '', 'lat' => '-6.764191', 'long' => '108.132599', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 9, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Ujungjaya', 'profil' => '', 'lat' => '-6.7297918', 'long' => '108.1013637', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 9, 'jml_kelurahan' => 0),
        //     array('nama_kecamatan' => 'Wado', 'profil' => '', 'lat' => '-6.9809857', 'long' => '108.1072924', 'camat' => '', 'luas' => '', 'jml_penduduk' => '', 'jml_desa' => 11, 'jml_kelurahan' => 0),
        // ));
        
        // TABLE DESA?KELURAHAN
        DB::table('villages')->insert(array(
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Buahdua', 'jml_penerima_bantuan' => '12', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Ciawitali', 'jml_penerima_bantuan' => '43', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Bojongloa', 'jml_penerima_bantuan' => '11', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Cibitung', 'jml_penerima_bantuan' => '4', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Cikurubuk', 'jml_penerima_bantuan' => '65', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Cilangkap', 'jml_penerima_bantuan' => '33', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Citaleus', 'jml_penerima_bantuan' => '7', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Gendereh', 'jml_penerima_bantuan' => '2', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Hariang', 'jml_penerima_bantuan' => '14', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Karangbungur', 'jml_penerima_bantuan' => '32', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Mekarmukti', 'jml_penerima_bantuan' => '33', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Nagrak', 'jml_penerima_bantuan' => '70', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Panyindangan', 'jml_penerima_bantuan' => '23', 'keterangan' => '-'),
            array('id_subdistrict' => 2, 'nama_desa' => 'Desa Sekarwangi', 'jml_penerima_bantuan' => '21', 'keterangan' => '-'),
            array('id_subdistrict' => 3, 'nama_desa' => 'Desa Buana Mekar', 'jml_penerima_bantuan' => '32', 'keterangan' => '-'),
            array('id_subdistrict' => 3, 'nama_desa' => 'Desa Cibugel', 'jml_penerima_bantuan' => '11', 'keterangan' => '-'),
            array('id_subdistrict' => 3, 'nama_desa' => 'Desa Ciapasang', 'jml_penerima_bantuan' => '22', 'keterangan' => '-'),
            array('id_subdistrict' => 3, 'nama_desa' => 'Desa Jaya Mekar', 'jml_penerima_bantuan' => '41', 'keterangan' => '-'),
            array('id_subdistrict' => 3, 'nama_desa' => 'Desa Jayamandiri', 'jml_penerima_bantuan' => '65', 'keterangan' => '-'),
            array('id_subdistrict' => 3, 'nama_desa' => 'Desa Sukaraja', 'jml_penerima_bantuan' => '36', 'keterangan' => '-'),
            array('id_subdistrict' => 3, 'nama_desa' => 'Desa Tamansari', 'jml_penerima_bantuan' => '11', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Cibeureum Kulon', 'jml_penerima_bantuan' => '9', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Cibeureum Wetan', 'jml_penerima_bantuan' => '11', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Cikole', 'jml_penerima_bantuan' => '10', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Cimalaka', 'jml_penerima_bantuan' => '3', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Cimuja', 'jml_penerima_bantuan' => '5', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Citimun', 'jml_penerima_bantuan' => '7', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Galudra', 'jml_penerima_bantuan' => '12', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Licin', 'jml_penerima_bantuan' => '77', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Mandalaherang', 'jml_penerima_bantuan' => '11', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Naluk', 'jml_penerima_bantuan' => '21', 'keterangan' => ''),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Nyalindung', 'jml_penerima_bantuan' => '14', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Padasari', 'jml_penerima_bantuan' => '15', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Serang', 'jml_penerima_bantuan' => '16', 'keterangan' => '-'),
            array('id_subdistrict' => 4, 'nama_desa' => 'Desa Trunamangala', 'jml_penerima_bantuan' => '12', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Cihanjuang', 'jml_penerima_bantuan' => '12', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Cikahuripan', 'jml_penerima_bantuan' => '11', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Cimanggung', 'jml_penerima_bantuan' => '9', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Mangunarga', 'jml_penerima_bantuan' => '32', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Pasirnunjang', 'jml_penerima_bantuan' => '22', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Sawahdadap', 'jml_penerima_bantuan' => '54', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Sindanggalih', 'jml_penerima_bantuan' => '22', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Sindangpakuon', 'jml_penerima_bantuan' => '22', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Sindulang', 'jml_penerima_bantuan' => '12', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Sukadana', 'jml_penerima_bantuan' => '6', 'keterangan' => '-'),
            array('id_subdistrict' => 5, 'nama_desa' => 'Desa Tegalmanggung', 'jml_penerima_bantuan' => '2', 'keterangan' => '-'),
            array('id_subdistrict' => 6, 'nama_desa' => 'Desa Bantarmara', 'jml_penerima_bantuan' => '31', 'keterangan' => '-'),
            array('id_subdistrict' => 6, 'nama_desa' => 'Desa Cimara', 'jml_penerima_bantuan' => '11', 'keterangan' => '-'),
            array('id_subdistrict' => 6, 'nama_desa' => 'Desa Cipandanwangi', 'jml_penerima_bantuan' => '12', 'keterangan' => '-'),
            array('id_subdistrict' => 6, 'nama_desa' => 'Desa Cisalak', 'jml_penerima_bantuan' => '7', 'keterangan' => '-'),
            array('id_subdistrict' => 6, 'nama_desa' => 'Desa Cisarua', 'jml_penerima_bantuan' => '8', 'keterangan' => '-'),
            array('id_subdistrict' => 6, 'nama_desa' => 'Desa Ciuyah', 'jml_penerima_bantuan' => '4', 'keterangan' => ''),
            array('id_subdistrict' => 6, 'nama_desa' => 'Desa Kebonkalapa', 'jml_penerima_bantuan' => '1', 'keterangan' => '-'),
        ));
    }
}
