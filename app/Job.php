<?php

namespace App;
use App\Employee;
use App\SubDistrict;
use App\Village;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $guarded = [];

    public function getEmployee()
    {
        return $this->belongsTo(Employee::Class, 'nik_pegawai', 'nik');
    }

    public function getHumanResource()
    {
        return $this->belongsTo(HumanResource::class, 'id_humanresource', 'id');
    }
    
    public function getDistrict()
    {
        return $this->belongsTo(SubDistrict::Class, 'id_district', 'id');
    }

    public function getVillage()
    {
        return $this->belongsTo(Village::Class, 'id_desa', 'id');
    }
}
