@extends('layouts.admin')

@section('title')
    Update URL Google Drive
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>
            
        <div class="col-md-12 grid-margin stretch-card">
                
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Download | Update URL Google Drive</h4>
                <form class="forms-sample" action="{{ route('download.update', $download->id) }}" method="POST">
                    @method('PATCH') 
                    {{ csrf_field() }}
                    <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                        <label for="url">URL Google Drive</label>
                        <input type="text" class="form-control" id="url" name="url" placeholder="URL Google Drive" value="{{ $download->url }}">
                        @if ($errors->has('url'))
                        <small class="text-danger">{{ $errors->first('url') }}</small>@endif
                    </div>
                    </div>
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="{{ route('download.index') }}"><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

