@extends('layouts.admin')

@section('title')
    Update Profil
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>
            
        <div class="col-md-12 grid-margin stretch-card">
                
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Profil | Update Profil</h4>
                <form class="forms-sample" action="{{ route('profile.update', $profile->id) }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH') 
                    {{ csrf_field() }}
                    <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                        <label for="jenis">Jenis Profile</label>
                        <input type="hidden" value="{{ $profile->jenis_profile }}" name="jenis_profile">
                        <input type="text" class="form-control" id="jenis" name="jenis" value="{{ $profile->jenis_profile }}" disabled>
                        @if ($errors->has('jenis'))
                        <small class="text-danger">{{ $errors->first('jenis') }}</small>@endif
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Gambar <small><cite title="Source Title">[opsional]</cite></small></label>
                            <div class="input-group col-xs-12">
                                <input type="file" name="image" id="profile-img" class="form-control file-upload-info" placeholder="Gambar/Foto Produk" value="{{ old('image') }}">
                            </div>
                            @if ($errors->has('image'))
                            <small class="text-danger">{{ $errors->first('image') }}</small>@endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Preview</label><br>
                            @if(isset($profile->image))
                            <img src="{{ asset('asset_general/images/profile/'.$profile->image.'') }}" id="profile-img-tag" width="100px" height="100px" />
                            @else
                            <img src="{{ asset('asset_general/images/news/preview.png') }}" id="profile-img-tag" width="100px" height="100px" />
                            @endif
                        </div>
                    </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="isi">Isi</label>
                        <textarea class="form-control" id="isi" rows="10" name="isi">{{ $profile->isi }}</textarea>
                        @if ($errors->has('isi'))
                        <small class="text-danger">{{ $errors->first('isi') }}</small>@endif
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href=""><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });

</script>


@endpush