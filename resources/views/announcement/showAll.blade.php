
@extends('layouts.frontend')

@section('title')
    Pengumuman
@endsection

@section('content')
<div class="container">
    <br>
        <div class="section">
            <div class="row">
                <div class="section health-section col-lg-9">
                    <h1 class="section-title">Pengumuman</h1>
                        @foreach($announcement as $ann)
                        <div class="health-feature">
                            <div class="post">
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        <img class="img-fluid" src="{{ asset('asset_general/images/announcements.png') }}" alt="" />
                                    </div>
                                </div>
                                <div class="post-content">								
                                    <div class="entry-meta">
                                        <ul class="list-inline">
                                            <li class="publish-date"><i class="fa fa-clock-o"></i> {{ $ann->created_at }} </li>
                                        </ul>
                                    </div>
                                    <h2 class="entry-title">
                                        <a href="{{ route('announcement.showById', $ann->id) }}">{{ $ann->judul }}</a>
                                    </h2>
                                </div>
                            </div><!--/post--> 
                        </div>
                        @endforeach
                        <div class="pagination-wrapper">
                        @if ($announcement->hasPages())
                        <ul class="pagination">
                            @if ($announcement->onFirstPage())
                                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                                    <span aria-hidden="true">&lsaquo;</span>
                                </li>
                            @else
                                <li ><a href="{{ $announcement->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"><span aria-hidden="true"><i class="fa fa-long-arrow-left"></i> Previous Page</span></a></li>
                            @endif
    
                            @foreach ($announcement as $ann)
                                @if (is_string($ann))
                                    <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                                @endif
    
                                @if (is_array($ann))
                                    @foreach ($ann as $page => $url)
                                        @if ($page == $announcement->currentPage())
                                            <li class="active" aria-current="page"><span>{{ $page }}</span></li>
                                        @else
                                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                            
                            @if ($announcement->hasMorePages())
                                <li><a href="{{ $announcement->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"><span aria-hidden="true">Next Page <i class="fa fa-long-arrow-right"></i></span></a></li>
                            @else
                                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                                    <span aria-hidden="true">&rsaquo;</span>
                                </li>
                            @endif
                        </ul>
                        @endif
                    </div>
                </div><!--/#content--> 
            
            @include('layouts.frontend_sidebar')
        </div>				
    </div><!--/.section-->
    </div><!--/.container-->
@endsection

