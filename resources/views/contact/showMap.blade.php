
@extends('layouts.frontend')

@section('title')
    Kontak
@endsection

@section('content')
<div class="container">
    <br>
    <h1 class="section-title title">Kontak Kami</h1>
    <div class="contact-us contact-page-two">
        <div class="map-section">
            <div id="gmap"></div>
        </div>
        <div class="contact-info">	
            <h1 class="section-title title">Informasi Kontak</h1>
            <ul class="list-inline">
                <li>
                    <h2>Alamat</h2>
                    <address>
                        {{ $contact->alamat }}
                    </address>
                </li>
                <li>
                   <h2>E-mail & Telp</h2>
                   <hr>
                    <address>
                        <p class="contact-mail"><strong>Email:</strong> {{ $contact->email }}</p>
                        
                        <p><strong>Telp:</strong> {{ $contact->no_telp }}</p>
                    </address>
                </li>
                <li>
                   <h2>Sosial Media</h2>
                   <hr>
                    <address>
                        <p class="contact-mail"><strong>Whatsapp:</strong> {{ $contact->wa }}</p>
                        
                        <p><strong>Instagram:</strong> {{ $contact->ig }}</p>
                    </address>
                </li>
            </ul>
        </div>
        <div class="message-box">
            <h1 class="section-title title">Drop Your Message</h1>
            <form id="comment-form" name="comment-form" method="post">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="subject" name="subject" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="comment" >Your Text</label>
                            <textarea name="comment" id="comment" required="required" class="form-control" rows="5"></textarea>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Send </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- contact-us -->
    </div><!--/.container-->
@endsection

@push('scripts')
<script>
    (function(){

        var map;

        map = new GMaps({
            el: '#gmap',
            lat: {{ $contact->lat }},
            lng: {{ $contact->long }},
            scrollwheel:false,
            zoom: 15,
            zoomControl : true,
            panControl : false,
            streetViewControl : true,
            mapTypeControl: true,
            overviewMapControl: true,
            clickable: true
        });

        var image = '';
        map.addMarker({
            lat: {{ $contact->lat }},
            lng: {{ $contact->long }},
            icon: image,
            animation: google.maps.Animation.DROP,
            verticalAlign: 'bottom',
            horizontalAlign: 'center',
            backgroundColor: '#d3cfcf',
             infoWindow: {
                content: '<div class="map-info"><address>{{$contact->nama_kecamatan}}</address></div>',
                borderColor: 'red',
            }
        });
          
        var styles = [ 

            {
              "featureType": "road",
              "stylers": [
                { "color": "#c1c1c1" }
              ]
              },{
              "featureType": "water",
              "stylers": [
                { "color": "#f1f1f1" }
              ]
              },{
              "featureType": "landscape",
              "stylers": [
                { "color": "#e3e3e3" }
              ]
              },{
              "elementType": "labels.text.fill",
              "stylers": [
                { "color": "#808080" }
              ]
              },{
              "featureType": "poi",
              "stylers": [
                { "color": "#dddddd" }
              ]
              },{
              "elementType": "labels.text",
              "stylers": [
                { "saturation": 1 },
                { "weight": 0.1 },
                { "color": "#7f8080" }
              ]
            }
      
        ];

    map.addStyle({
            styledMapName:"Styled Map",
            styles: styles,
            mapTypeId: "map_style"  
        });

        map.setStyle("map_style");
    }());
</script>
@endpush
