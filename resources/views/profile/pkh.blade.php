
@extends('layouts.frontend')

@section('title')
    Profil
@endsection

@section('content')
<div class="container">
    <br>
        <div class="section">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div class="section world-news">
                        <h1 class="section-title title">Profil PKH</h1>	
                        <div class="world-nav cat-menu">         
                            <ul class="list-inline">                      
                                <li class="active"><a href="{{ route('profile.pkh') }}">Profil</a></li>
                                <li><a href="{{ route('profile.visimisi') }}">Visi & Misi</a></li>
                                <li><a href="{{ route('profile.tugasfungsi') }}">Tugas & Fungsi</a></li>
                            </ul> 					
                        </div>
                        <div class="post">
                                @if(isset($profiles))
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        @if(isset($profiles->image))
                                        <img class="img-fluid" height="350" width="870" src="{{ asset('asset_general/images/profile/'.$profiles->image.'')}}" alt="" />
                                        @endif
                                    </div>
                                </div>
                                <div class="post-content">
                                    <div class="entry-content">
                                        <p>{!! nl2br(e($profiles->isi)) !!}</p>
                                    </div>
                                </div>
                                @else
                                <div class="col-lg-12" style="margin-top: -3px">
                                        <div class="left-content">
                                            <div class="details-news">											
                                                <div class="post">
                                                    <div class="post-content">								
                                                        <div class="entry-content" align="center">
                                                            <h3>-- Belum Ada Data --</h3>
                                                        </div>
                                                    </div>
                                                </div><!--/post--> 
                                            </div><!--/.section-->
                                        </div><!--/.left-content-->
                                    </div>
                                @endif
                            </div><!--/post--> 
                    </div><!--/.section-->
                </div>
                @include('layouts.frontend_sidebar')
            </div>				
        </div><!--/.section-->
    </div><!--/.container-->
@endsection