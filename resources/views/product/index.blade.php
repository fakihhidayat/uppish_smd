@extends('layouts.admin')

@section('title')
    Produk Ekonomi
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div>
            @endif
      </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="row">
                <div class="col-lg-8"><h4 class="card-title">Produk Ekonomi</h4></div>
                <div class="col-lg-4" align="right">
                    <a href="{{ route('product.create')}}">
                        <button type="button" class="btn btn-primary btn-fw">
                        <i class="mdi mdi-plus"></i>Tambah</button>
                    </a>
                </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped" id="datatable-product">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Gambar</th>
                        <th>Nama Produk</th>
                        <th>Harga</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php $no = 1; ?>
                       @if(count($products) > 0)
                       @foreach ($products as $pro)
                       <tr>
                            <td>{{ $no++ }}</td>
                            <td class="py-1">
                                <img src="{{ asset('asset_general/images/product/'.$pro->foto.'') }}" alt="image" />
                            </td>
                            <td>{{ $pro->nama_produk }}</td>
                            <td>Rp {{ $pro->harga }} /{{ $pro->jual_per }}</td>
                            <td><select class="form-control">
                                    <option value="published">{{ $pro->status }}</option>
                                    @if($pro->status == 'sale')
                                        <option value="gudang">Gudang-kan</option>
                                    @else 
                                        <option value="sale">Jual</option>
                                    @endif
                                </select>
                            </td>
                            <td><div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" title="Ubah" class="btn btn-dark btn-sm"><a href="{{ route('product.edit', $pro->id) }}" style="color: white"><i class="mdi mdi-pencil"></i></a></button>
                                <form action="{{ route('product.destroy', $pro->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" title="Hapus" class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                                </form>
                              </div>
                          </td>
                        </tr>
                       @endforeach 
                       @else
                        <tr class="bg-secondary">
                          <td colspan="6" align="center">-- Tidak Ada Data --</td>
                        </tr>
                       @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

@endsection