@extends('layouts.admin')

@section('title')
    Post Info Baru
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>

        <div class="col-md-12 grid-margin stretch-card" id="myForm">
                
            <div class="card">
                <div class="card-body">
                    <form id="kategori-form" method="POST" action="javascript:void(0)">
                        @csrf
                    <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                            <label for="kategori_new">Nama Kategori</label>
                            <input type="text" class="form-control" id="kategori_new" name="kategori_new" placeholder="Nama Kategori Baru">
                            @if ($errors->has('kategori_new'))
                            <small class="text-danger">{{ $errors->first('kategori_new') }}</small>@endif
                        </div>
                        </div>
                        <div class="col-md-8">
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi</label>
                            <textarea class="form-control" id="deskripsi" name="deskripsi" rows="4"></textarea>
                        </div>
                        </div>
                        <div class="alert alert-success d-none" id="msg_div">
                            <span id="res_message"></span>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <button type="submit" class="btn btn-success mr-2 btn-save" id="send_form">Submit</button>
                            <button class="btn btn-light btn-slide">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
        
            
        <div class="col-md-12 grid-margin stretch-card">
                
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Berita | Post Info Baru</h4>
                <form class="forms-sample" action="{{ route('news.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Info" value="{{ old('judul') }}">
                        @if ($errors->has('judul'))
                        <small class="text-danger">{{ $errors->first('judul') }}</small>@endif
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="kategori">Kategori</label>
                                {!! Form::select('kategori', $kategori, null, ['class'=>'form-control', 'placeholder'=> '-- Kategori Info --', 'value' => '{{ old("kategori")}}']) !!}
                                @if ($errors->has('kategori'))
                                <small class="text-danger">{{ $errors->first('kategori') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <br>
                                <button type="button" class="btn btn-outline-primary btn-slide">
                                <i class="mdi mdi-plus"></i> Katgeori Baru</button>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Gambar/Foto</label>
                                <div class="input-group col-xs-12">
                                    <input type="file" name="image" id="profile-img" class="form-control file-upload-info" placeholder="Gambar/Foto Info" value="{{ old('image') }}">
                                </div>
                                @if ($errors->has('image'))
                                <small class="text-danger">{{ $errors->first('image') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-2">
                                <div class="form-group">
                                    <label>Preview</label><br>
                                    <img src="{{ asset('asset_general/images/news/preview.png') }}" id="profile-img-tag" width="100px" height="100px" />
                                </div>
                            </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="uraian">Uraian</label>
                        <textarea class="form-control" id="uraian" rows="30" name="uraian">{{ old('uraian')}}</textarea>
                        @if ($errors->has('uraian'))
                        <small class="text-danger">{{ $errors->first('uraian') }}</small>@endif
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="{{ route('news.index') }}"><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
</script>

<script>
$(document).ready(function() {
 
 $("#myForm").hide();

 $('.card').on("click", '.btn-slide', function() {
     $("#myForm").slideToggle("slow");
 });

});


if ($("#kategori-form").length > 0) {
         $("#kategori-form").validate({
           
         rules: {
           kategori_new: {
             required: true
           },  
         },
         messages: {
             
           kategori_new: {
             required: "Nama Kategori harus diisi !"
           },
              
         },
         submitHandler: function(form) {
          $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
           });
           $('#send_form').html('Sending..');
           $.ajax({
             url: '{{ route("category.storeNew") }}',
             type: "PUT",
             data: $('#kategori-form').serialize(),
             success: function(response) {
                 $('#send_form').html('Submit');
                 $('#res_message').show();
                 $('#res_message').html(response.msg);
                 $('#msg_div').removeClass('d-none');
      
                 document.getElementById("kategori-form").reset(); 
                 setTimeout(function(){
                 $('#res_message').hide();
                 $('#msg_div').hide();
                 },10000);
             }
           });
         }
       })
     }
        
</script>

@endpush