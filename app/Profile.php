<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable =
    [
        'jenis_profile','isi','image'
    ];

    public $timestamps = false;
}
