@extends('layouts.admin')

@section('title')
    Kegiatan 
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div>
            @endif
      </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="row">
                <div class="col-lg-8"><h4 class="card-title font-weight-bold">Kegiatan</h4></div>
                <div class="col-lg-4" align="right">
                    <a href="{{ route('activity.create')}}">
                        <button type="button" class="btn btn-primary btn-fw">
                        <i class="mdi mdi-plus"></i>Tambah</button>
                    </a>
                </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Judul</th>
                        <th>Lokasi & Tanggal Kegiatan</th>
                        <th>Status</th>
                        <th>#</th>
                        <th>Tanggal</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody> 
                      @if(count($group) > 0)
                       @foreach($group as $category => $activity)
                        <tr>
                          <td colspan="7" class="table-primary" align="center"><strong>{{ $category }}</strong></td>
                        </tr>
                       <?php $no = 1 ?>
                       @foreach ($activity as $act)
                       <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $act->judul }}</td>
                            <td>{{ $act->lok_kegiatan }} | {{ date('d, F Y', strtotime($act->tgl_kegiatan)) }}</td>
                            <td><button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ $act->status }}
                              </button>
                              @if($act->status == 'published')
                              <div class="dropdown-menu">
                                <form action="{{ route('activity.updateStatus', $act->id) }}" method="post">
                                  @method('PATCH')
                                @csrf
                                <input type="hidden" name="status" value="drafted">
                                <button type="submit"><a class="dropdown-item">
                                  <i class="mdi mdi-eye-off"></i> Draft-kan</a></button>
                                </form>
                              </div>
                              @else 
                              <div class="dropdown-menu">
                                  <form action="{{ route('activity.updateStatus', $act->id) }}" method="post">
                                    @method('PATCH')
                                  @csrf
                                  <input type="hidden" name="status" value="published">
                                  <button type="submit"><a class="dropdown-item">
                                    <i class="mdi mdi-send"></i> Publish</a></button>
                                  </form>
                                </div>
                               @endif
                            </td>
                            <td>
                                <i class="mdi mdi-comment-text-outline mx-0"></i> {{ count($act->getComments) }} 
                                <br><i class="mdi mdi-eye mx-0"></i> 1.5k
                            </td>
                            <td>{{ $act->created_at }}</td>
                            <td><div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" title="Ubah" class="btn btn-dark btn-sm"><a href="{{ route('activity.edit', $act->id) }}" style="color: white"><i class="mdi mdi-pencil"></i></a></button>
                                <form action="{{ route('activity.destroy', $act->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" title="Hapus" class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                                </form>
                              </div>
                            </td>
                        </tr>
                       @endforeach 
                       @endforeach
                       @else
                        <tr class="bg-secondary">
                          <td colspan="7" align="center">-- Tidak Ada Data --</td>
                        </tr>
                       @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

@endsection
