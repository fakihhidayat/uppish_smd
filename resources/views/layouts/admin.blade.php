<?php
use Illuminate\Support\Facades\Auth;
use App\HumanResource;
use App\User;

$admin = User::where('id', Auth::id())->first();
$humanresources = HumanResource::All();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title') | Administrator</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('asset_admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('asset_admin/vendors/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('asset_admin/vendors/css/vendor.bundle.base.css') }}">
  <link rel="stylesheet" href="{{ asset('asset_admin/vendors/css/vendor.bundle.addons.css') }}">
  <link rel="stylesheet" href="{{ asset('asset_admin/vendors/datepicker/css/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="{{ asset('asset_admin/summernote/summernote.css') }}">
  {{-- <link rel="stylesheet" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"> --}}

  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('asset_admin/css/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('asset_admin/images/favicon.png') }}" />

  @stack('styles')
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html">
          <img src="{{ asset('asset_admin/images/logo2.png') }}" alt="logo" width="20px"/>
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html">
          <img src="{{ asset('asset_admin/images/logo-mini.svg') }}" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
          <li class="nav-item">
            <a href="{{ route('profile.index') }}" class="nav-link">Profil Lembaga
              <span class="badge badge-primary ml-1">New</span>
            </a>
          </li>
          <li class="nav-item active">
            <a href="{{ route('subdistrict.index') }}" class="nav-link">
              <i class="mdi mdi-elevation-rise"></i>Kecamatan & Desa</a>
          </li>

        </ul>
        <ul class="navbar-nav navbar-nav-right">

          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello, @if(isset($admin)) {{ $admin->name }} ! @endif</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item mt-2">
                Pengaturan Akun
              </a>
              <a href="" class="dropdown-item">
                Ubah Kata Sandi
              </a>
              <a href="{{ route('logout') }}" class="dropdown-item">
                Keluar
              </a>
              {{-- <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
              </form> --}}
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="{{ asset('asset_admin/images/faces/face1.jpg') }}" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">{{ $admin->name }}</p>
                  <div>
                    <small class="designation text-muted">@if($admin->admin == 1) Administrator @endif</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">
              <i class="menu-icon mdi mdi-microsoft"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-human-handsup"></i>
              <span class="menu-title">SDM</span>
              <i class="menu-arrow"></i>
            </a>
            @if(isset($humanresources))
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                @foreach ($humanresources as $hr)
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('humanresource.show', $hr->id) }}">{{ $hr->nama_sdm }}</a>
                </li>
                @endforeach
              </ul>
            </div>
            @else
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="">Tambah SDM</a>
                </li>
              </ul>
            </div>
            @endif
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#peserta" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-human-handsup"></i>
              <span class="menu-title">Peserta PKH</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="peserta">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('participant.index') }}">Daftar Peserta PKH</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('participant.showDataTahapI') }}">Data Per-Kecamatan</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pages/ui-features/typography.html">
              <i class="menu-icon mdi mdi-alert-circle"></i>
              <span class="menu-title">Pengaduan</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('activity.index') }}">
              <i class="menu-icon mdi mdi-backup-restore"></i>
              <span class="menu-title">Kegiatan</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('announcement.index') }}">
              <i class="menu-icon mdi mdi-alert-circle"></i>
              <span class="menu-title">Pengumuman</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#news" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-newspaper"></i>
              <span class="menu-title">Berita</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="news">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('news.index') }}">Info Terbaru</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="pages/ui-features/typography.html">Catatan Pendamping</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('product.index') }}">
              <i class="menu-icon mdi mdi-cube-send"></i>
              <span class="menu-title">Pemberdayaan Ekonomi</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('video.index') }}">
              <i class="menu-icon mdi mdi-youtube-play"></i>
              <span class="menu-title">Video</span>
            </a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="{{ route('download.index') }}">
                <i class="menu-icon mdi mdi-download"></i>
                <span class="menu-title">Download</span>
              </a>
            </li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        @yield('content')
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        @include('layouts._modalSmall')
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
              <a href="http://www.bootstrapdash.com/" target="_blank">PKH Sumedang</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">made with
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>

  <script src="{{ asset('js/newJs.js') }}"></script>
  <script src="{{ asset('asset_admin/vendors/js/vendor.bundle.base.js') }}"></script>
  <script src="{{ asset('asset_admin/vendors/js/vendor.bundle.addons.js') }}"></script>
  <script src="{{ asset('asset_admin/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('asset_admin/vendors/datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('asset_admin/summernote/summernote.js') }}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{ asset('asset_admin/js/off-canvas.js') }}"></script>
  <script src="{{ asset('asset_admin/js/misc.js') }}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{ asset('asset_admin/js/dashboard.js') }}"></script>
  <!-- End custom js for this page-->
  <script>
       $(document).ready(function() {
            $('.summernote-editor').summernote();
       });
  </script>

  @stack('scripts')

</body>

</html>
