<?php
use App\SubDistrict;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;

Route::get('/', 'WelcomeController@index')->name('/');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::group(['middleware' => ['web','auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::get('/home', function(){
    if(Auth::user()->admin == 0)
    {
        return redirect()->action('WelcomeController@index');
    } 
    else 
    {
        return view('admin');
    }
})->name('home');

/* Announcement/Pengumuman */
Route::group(['prefix' => 'announcement'], function(){
    Route::get('/showAll', 'AnnouncementController@showAll')->name('announcement.showAll');
    Route::get('/showById/{id}', 'AnnouncementController@showById')->name('announcement.showById');
});
Route::resource('announcement', 'AnnouncementController');

/* Profile */
Route::group(['prefix' => 'profile'], function () {
    Route::get('/pkh', 'ProfileController@showPkh')->name('profile.pkh');
    Route::get('/visimisi', 'ProfileController@showVisiMisi')->name('profile.visimisi');
    Route::get('/tugasfungsi', 'ProfileController@showTugasFungsi')->name('profile.tugasfungsi'); 
    Route::get('/createNew/{jenis}', 'ProfileController@createNew')->name('profile.createNew');
});

Route::resource('profile', 'ProfileController');

/* Human Resource/SDM */
Route::resource('humanresource', 'HumanResourceController');
Route::get('/humanresource/detail/{id}', 'HumanResourceController@detail')->name('humanresource.detail');

/* Sub District/Kecamatan */
Route::resource('subdistrict', 'SubDistrictController');

/* Activity/Kegiatan */
Route::resource('activity', 'ActivityController');
Route::get('/activity/showById/{id}', 'ActivityController@showById')->name('activity.showById');
Route::patch('activity/updateStatus/{id}', 'ActivityController@updateStatus')->name('activity.updateStatus');

/* Comment/Komentar Kegiatan */
Route::resource('activitycomment', 'ActivityCommentController');

/* News/Berita */


Route::group(['prefix' => 'news'], function () {
    Route::get('/showAll', 'NewsController@showAll')->name('news.showAll');
    Route::get('/showById/{id}', 'NewsController@showById')->name('news.showById');
    Route::patch('/updateStatus/{id}', 'NewsController@updateStatus')->name('news.updateStatus');
});
Route::resource('news', 'NewsController');
Route::post('news/storeCategory', 'NewsController@storeCategory')->name('news.storeCategory');

/* Category/Kategori */
Route::patch('/category/storeNew', 'CategoryController@storeNew')->name('category.storeNew');
Route::resource('category', 'CategoryController');

/* Comment/Komentar Berita */
Route::resource('newscomment', 'NewsCommentController');

/* Product/Pemberdayaan Ekonomi (Produk) */
Route::get('/product/showAll', 'ProductController@showAll')->name('product.showAll');
Route::resource('product', 'ProductController');

/* Complaint/Sistem Pengaduan */
Route::group(['prefix' => 'complaint'], function () {
    Route::get('/formComplaint', 'ComplaintController@form')->name('complaint.formComplaint');
    Route::get('/getVillage/{id}', 'ComplaintController@getVillage')->name('complaint.getVillage');
});
Route::resource('complaint', 'ComplaintController');

/* Kontak/Contact */
Route::get('/contact/map', 'ContactController@showMap')->name('contact.map');
Route::resource('contact', 'ContactController');

/* Employee/Karyawan */
// Route::group(['prefix' => 'employee'], function () {
//     Route::get('/listByDistrict', 'EmployeeController@listByDistrict')->name('employee.listByDistrict');
//     Route::get('/detail/{id}', 'EmployeeController@detail')->name('employee.detail'); 
// });
Route::get('/employee/detail/{id}', 'EmployeeController@detail')->name('employee.detail');
Route::get('/employee/createNew/{id}', 'EmployeeController@createNew')->name('employee.createNew');
Route::get('/employee/getVillage/{id}', 'EmployeeController@getVillage')->name('employee.getVillage');
Route::resource('employee', 'EmployeeController');

/* Video */
Route::resource('video', 'VideoController');

/* Village */
Route::get('/village/createNew/{id}', 'VillageController@createNew')->name('village.createNew');
Route::resource('village', 'VillageController');

/* KPM atau PESERTA PKH */
Route::group(['prefix' => 'participant'], function () {
    Route::get('/showDataTahapI', 'ParticipantController@showDataTahapI')->name('participant.showDataTahapI');
    Route::get('/getVillage/{id}', 'ParticipantController@getVillage')->name('participant.getVillage');
});
Route::resource('participant', 'ParticipantController');

/* Download */
Route::get('/download/showDownload', 'DownloadController@showDownload')->name('download.showDownload');
Route::resource('download', 'DownloadController');
Route::get('/download/{uuid}/download', 'DownloadController@download')->name('download.download');



