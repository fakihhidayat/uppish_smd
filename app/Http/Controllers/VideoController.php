<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\NewsCategory;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $video = Video::orderBy('created_at', 'desc')->with('getVideoCategory')->paginate(10);
        return view('video.index', compact('video'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = NewsCategory::All()->sortBy('nama_kategori', SORT_NATURAL | SORT_FLAG_CASE)->pluck('nama_kategori', 'id');
        return view('video.formCreate', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|string|max:255|unique:videos,judul', 
            'kategori' => 'required',
            'url' => 'required'
        ]);

        $video = new Video([
            'judul' => $request->get('judul'),
            'id_kategori' => $request->get('kategori'),
            'url_youtube' => $request->get('url')
        ]);
        $video->save();
        return back()->with('success','Video Baru berhasil ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::where('id', $id)->first();
        $kategori = NewsCategory::orderBy('nama_kategori', 'asc')->get();
        return view('video.formEdit', compact(['video','kategori'])); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|string|max:255', 
            'kategori' => 'required',
            'url' => 'required'
        ]);

        $video = Video::find($id);
        $video->judul = $request->get('judul');
        $video->id_kategori = $request->get('kategori');
        $video->url_youtube = $request->get('url');

        try {
            $video->save();
            return redirect()->route('video.index')->with('success','Video berhasil diupdate !');
        }
        catch (\Exception $e) { 
            return back()->with('error','Video gagal diupdate !');
        }         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Video::destroy($id);
        return back()->with('success', 'Video berhasil dihapus!');
    }
}
