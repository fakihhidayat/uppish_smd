
@extends('layouts.frontend')

@section('title')
    Sistem Pengaduan
@endsection

@section('content')
<br>
<div class="container">
    <div class="section">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div id="site-content" class="site-content">
                            <div class="section world-news">
                                <h1 class="section-title title">Sistem Pengaduan</h1>
                                <div class="flash-message">
                                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                    @if(Session::has('alert-' . $msg))
                            
                                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                    @endif
                                @endforeach
                                </div>	
                                <div class="post">
                                    <div class="post-content">
                                        <div class="entry-content">
                                            <h2><b>PERINGATAN !</b></h2>
                                        </div>
                                    </div>
                                    <div class="list-post">
                                        <ul>
                                            <li><a>Gunakan Sistem Pengaduan Ini Dengan Bijak & Benar Adanya Sehingga Isi Pesan Pengaduan Dapat Dipertanggungjawabkan Dikemudian Hari</a></li>
                                            <li><a>Isi Form Pengaduan Dengan Menggunakan Identitas Asli</a></li>
                                            <li><a>Harap Isi Semua Kolom Yang Tersedia</a></li>
                                        </ul>
                                    </div><!--/list-post--> 
                                </div><!--/post--> 
                            </div><!--/.section-->
                    </div><!--/#site-content-->
                    
                    <div class="row">
                        <div class="col-sm-12">	
                                <div class="comments-box">
                                    <h1 class="section-title title">Form Pengaduan</h1>
                                    <form id="comment-form" name="comment-form" method="post" action="{{ route('complaint.store') }}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name">Nama Lengkap</label>
                                                    <input type="text" name="name" class="form-control" required="required">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="nik">NIK KTP (Kartu Tanda Penduduk)</label>
                                                    <input type="text" name="nik" class="form-control" required="required" onkeypress="return hanyaAngka(event)" maxlength="16">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="phone">Nomor HP</label>
                                                    <input type="text" name="phone" class="form-control" required="required" onkeypress="return hanyaAngka(event)"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="district">Kecamatan</label>
                                                    <select name="district" id="district" class="form-control" required>
                                                        <option value="">--Pilih Kecamatan--</option> 
                                                        @foreach ($districts as $key => $value)
                                                            <option value="{{ $key }}">{{ $value }}</option> 
                                                        @endforeach
                                                    </select>   
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="village">Desa/Kelurahan</label>
                                                    <select name="village" id="village" class="form-control" required>
                                                        <option value="">--Pilih Desa/Kelurahan--</option> 
                                                    </select>   
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="address">Alamat</label>
                                                    <input type="text" name="address" class="form-control" required="required">
                                                </div>
                                            </div><div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="complaint">Topik Pengaduan</label>
                                                    <textarea name="complaint" id="complaint" required="required" class="form-control" row="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="pesan">Pesan</label>
                                                    <textarea name="pesan" id="pesan" required="required" class="form-control"></textarea>
                                                </div>
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-primary">Kirim </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            	
                        </div>
                    </div>
                </div><!--/.col-sm-9 -->	
                
                @include('layouts.frontend_sidebar')

            </div>				
        </div><!--/.section-->
    </div><!--/.container-->
@endsection

@push('scripts')
<script>
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function()
    {
        jQuery('select[name="district"]').on('change', function(){
            var districtID = jQuery(this).val();
            if(districtID)
            {
                jQuery.ajax({
                    url : 'getVillage/' +districtID,
                    type : "GET",
                    dataType : "json",
                    success:function(data)
                    {
                        console.log(data);
                        jQuery('select[name="village"]').empty();
                        jQuery.each(data, function(key,value){
                            $('select[name="village"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                    }
                });
            } else 
            {
                $('select[name="village"]').empty();
            }
        });
    });
</script>
@endpush

