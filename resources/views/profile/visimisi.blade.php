
@extends('layouts.frontend')

@section('title')
    Visi & Misi
@endsection

@section('content')
<div class="container">
    <br>
        <div class="section">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div class="section world-news">
                        <h1 class="section-title title">Visi Misi PKH</h1>	
                        <div class="world-nav cat-menu">         
                            <ul class="list-inline">                      
                                <li><a href="{{ route('profile.pkh') }}">Profil</a></li>
                                <li class="active"><a href="{{ route('profile.visimisi') }}">Visi & Misi</a></li>
                                <li><a href="{{ route('profile.tugasfungsi') }}">Tugas & Fungsi</a></li>
                            </ul> 					
                        </div>
                        <div class="post">
                            <div class="entry-header">
                                <div class="entry-thumbnail">
                                    @if(isset($visions->image) && isset($missions->image))
                                    <img class="img-fluid" height="350" width="870" src="{{ asset('asset_general/images/profile/'.$visions->image.'') }}" alt="" />
                                    @else
                                    @if(isset($visions->image))
                                    <img class="img-fluid" height="350" width="870" src="{{ asset('asset_general/images/profile/'.$visions->image.'') }}" alt="" />
                                    @endif
                                    @if(isset($missions->image))
                                    <img class="img-fluid" height="350" width="870" src="{{ asset('asset_general/images/profile/'.$missions->image.'') }}" alt="" />
                                    @endif
                                    @endif
                                </div>
                            </div>
                            <div class="post-content">	
                                @if(!isset($visions) && !isset($missions))
                                <div class="col-lg-12" style="margin-top: -3px">
                                    <div class="left-content">
                                        <div class="details-news">											
                                            <div class="post">
                                                <div class="post-content">								
                                                    <div class="entry-content" align="center">
                                                        <h3>-- Belum Ada Data --</h3>
                                                    </div>
                                                </div>
                                            </div><!--/post--> 
                                        </div><!--/.section-->
                                    </div><!--/.left-content-->
                                </div>
                                @else
                                @if(isset($visions))							
                                <h2 class="entry-title">
                                    {{ $visions->jenis_profile }}
                                </h2>
                                <div class="entry-content">
                                    <p>{{ $visions->isi }}</p>
                                </div>
                                <br>
                                @endif
                                @if(isset($missions))
                                <h2 class="entry-title">
                                    {{ $missions->jenis_profile }} 
                                </h2>
                                <div class="entry-content">
                                    <p>{!! nl2br(e($missions->isi)) !!}</p>
                                </div>
                                @endif
                                @endif
                            </div>        
                        </div><!--/post--> 
                    </div><!--/.section-->
                </div>
                @include('layouts.frontend_sidebar')
            </div>				
        </div><!--/.section-->
    </div><!--/.container-->
@endsection