<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cohensive\Embed\Facades\Embed;
use App\Announcement;
use App\Profile;
use App\ProfileCategory;
use App\HumanResource;
use App\NewsCategory;
use App\Video;

class WelcomeController extends Controller
{
    public function index()
    {
        $videos = Video::orderBy('created_at', 'desc')->with('getVideoCategory')->limit(3)->get();
        

        $announcements = Announcement::All();
        $profiles = Profile::where('jenis_profile', 'Selayang Pandang')->first();
        return view('welcome', compact(['announcements','profiles','videos']));
    }
    
}
