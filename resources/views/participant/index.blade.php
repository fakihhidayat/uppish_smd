@extends('layouts.admin')

@section('title')
    Peserta PKH 
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div>
            @endif
      </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="row">
                <div class="col-lg-10"><h4 class="card-title">Peserta PKH | Daftar Hasil Final Closing TAHAP I</h4></div>
                <div class="col-lg-2" align="right">
                    <a title="Tambah Berita | Info Terbaru" href="{{ route('participant.create') }}">
                            <button type="button" class="btn btn-primary btn-fw" style="margin-right: 0px">
                                    <i class="mdi mdi-plus"></i>Tambah</button>
                    </a>
                </div>
                </div>
                <div class="table-responsive-sm">
                  <table class="table table-sm table-bordered" id="datatable-news">
                    <thead>
                      <tr class="bg-secondary">
                        <th rowspan="2" style="vertical-align: middle !important;">No</th>
                        <th rowspan="2" style="vertical-align: middle !important;">Nomor PKH</th>
                        <th rowspan="2" style="vertical-align: middle !important;">Nama Pengurus</th>
                        <th rowspan="2" style="vertical-align: middle !important;">Alamat</th>
                        <th rowspan="2" style="vertical-align: middle !important;">Desa/Kel.</th>
                        <th rowspan="2" style="vertical-align: middle !important;">Kecamatan</th>
                        <th colspan="8" style="text-align: center;">Kategori KPM</th>
                        <th rowspan="2" style="vertical-align: middle !important;">Aksi</th>
                      </tr>
                        <tr style="text-align: center;" class="bg-secondary">
                          <th>Bumil</th>
                          <th>Balita</td>
                          <th>APRAS</td>
                          <th>SD</td>
                          <th>SMP</td>
                          <th>SMA</td>
                          <th>Lansia</td>
                          <th>Dis.</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php $no = 1; ?>
                       @if(count($peserta) > 0)
                       @foreach ($peserta as $p)
                       <tr>
                         <td><small>{{ $no++ }}</small></td>
                         <td><small>{{ $p->getParticipant->nomor_pkh }}</small></td>
                         <td><small>{{ $p->getParticipant->nama }}</small></td>
                         <td><small>{{ $p->getParticipant->alamat }}</small></td>
                         <td><small>{{ $p->getVillage->nama_desa }}</small></td>
                         <td><small>{{ $p->getDistrict->nama_kecamatan }}</small></td>
                         <td style="text-align: center;">{{ $p->bumil }}</td>
                         <td style="text-align: center;">{{ $p->balita }}</td>
                         <td style="text-align: center;">{{ $p->apras }}</td>
                         <td style="text-align: center;">{{ $p->sd }}</td>
                         <td style="text-align: center;">{{ $p->smp }}</td>
                         <td style="text-align: center;">{{ $p->sma }}</td>
                         <td style="text-align: center;">{{ $p->lansia }}</td>
                         <td style="text-align: center;">{{ $p->disabilitas }}</td>
                          <td><div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" title="Ubah" class="btn btn-dark btn-xs"><a href="" style="color: white"><i class="mdi mdi-pencil"></i></a></button>
                                <form action="" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" title="Hapus" class="btn btn-danger btn-xs"><i class="mdi mdi-delete"></i></button>
                                </form>
                              </div>
                          </td> 
                        </tr>
                       @endforeach
                       @else
                        <tr class="bg-secondary">
                          <td colspan="14" align="center">-- Tidak Ada Data --</td>
                        </tr>
                       @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

@endsection