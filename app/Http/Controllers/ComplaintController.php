<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubDistrict;
use App\Village;
use App\Complaint;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Complaint::create([
            'nama' => $request->name,
            'nik' => $request->nik,
            'id_desa' => $request->village,
            'alamat' => $request->address,
            'no_hp' => $request->phone, 
            'topik_pengaduan' => $request->complaint,
            'pesan' => $request->pesan
        ]);
        
        $request->session()->flash('alert-info', 'Pengaduan Anda Telah Terkirim');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function form()
    {
        $districts = SubDistrict::orderBy('nama_kecamatan', 'asc')->pluck('nama_kecamatan', 'id');

        return view('complaint.form', compact('districts'));
    }

    public function getVillage($id)
    {
        $village = Village::where('id_subdistrict', $id)->pluck("nama_desa","id");
        return json_encode($village);    
    }
}
