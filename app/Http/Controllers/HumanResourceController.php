<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HumanResource;
use App\Employee;
use App\Job;

class HumanResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hr = HumanResource::where('id', $id)->first();
        $employees = Job::where('id_humanresource', $id)->with('getEmployee')->get();
        if($id == 4 || $id == 5) {
            $jobs = Job::where('id_humanresource', $id)->with(['getDistrict', 'getEmployee'])->get();
            $group = $jobs->groupBy(function ($job){
            return $job->getDistrict->nama_kecamatan;
            });
            return view('humanresource.index', compact(['group','hr']));
        } else {
            return view('humanresource.index', compact(['employees','hr']));
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function detail($id)
    {
        $humanresource = HumanResource::where('id', $id)->first();
        return view('humanresource.detail', compact('humanresource'));
    }

}
