<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;
use App\NewsCategory;

class Video extends Model
{
    protected $fillable =
    [
        'id_kategori','judul','url_youtube'
    ];

    public function getVideoCategory()
    {
        return $this->belongsTo(NewsCategory::class, 'id_kategori', 'id');
    }

    public function getCreatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])->format('d, M Y');
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }
    
}
