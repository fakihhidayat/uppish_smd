<?php
use App\News;
use App\Activity;
use App\Product;
 
$news = News::orderBy('created_at', 'desc')->where('status', 'published')->limit(2)->get();
$activity = Activity::orderBy('created_at', 'desc')->limit(2)->get();
$product = Product::orderBy('created_at', 'desc')->limit(2)->get();
?>
<div class="container">
    <div id="main-slider">
        @if(count($news) > 0)
        @foreach ($news as $new)
        <div class="post feature-post" style="background-image:url({{ asset('asset_general/images/news/'.$new->image.'')}}); background-size:cover;"> 
            <div class="post-content">
                <div class="catagory world"><a href="{{ route('news.showAll') }}">Info Terbaru</a></div>
                <h2 class="entry-title">
                    <a href="{{ route('news.showById', $new->id) }}">{{ $new->judul }}</a>
                </h2>
            </div>
        </div><!--/post-->
        @endforeach
        @endif

        @if(count($activity) > 0)
        @foreach ($activity as $act)
        <div class="post feature-post" style="background-image:url({{ asset('asset_general/images/activity/'.$act->foto.'')}}); background-size:cover;">
            <div class="post-content">
                <div class="catagory"><a href="">Kegiatan Terbaru</a></div>
                <h2 class="entry-title">
                    <a href="{{ route('activity.showById', $act->id) }}">{{ $act->judul }}</a>
                </h2>
            </div>
        </div><!--/post-->
        @endforeach
        @endif

        @if(count($product) > 0)
        @foreach ($product as $pro)
        <div class="post feature-post" style="background-image:url({{ asset('asset_general/images/product/'.$pro->foto.'')}}); background-size:cover;">
            <div class="post-content">
                <div class="catagory politics"><a href="{{ route('product.showAll') }}">Produk Ekonomi Terbaru</a></div>
                <h2 class="entry-title">
                    <a href="{{ route('product.showAll') }}">{{ $pro->nama_produk }}</a>
                </h2>
            </div>
        </div><!--/post-->
        @endforeach
        @endif
    </div><!-- #main-slider -->
</div>
