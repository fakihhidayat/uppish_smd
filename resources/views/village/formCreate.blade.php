@extends('layouts.admin')

@section('title')
    Tambah Desa Baru
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>
        
            
        <div class="col-md-12 grid-margin stretch-card">
                
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Desa | Tambah Desa Baru</h4>
                <form class="forms-sample" action="{{ route('village.store') }}" method="POST">
                    {{ csrf_field() }}
                    
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" name="district" value="{{ $request->id }}">
                            <div class="form-group">
                                <label for="kecamatan">Nama Kecamatan</label>
                                <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="Nama Kecamatan" value="{{ $request->nama_kecamatan }}" disabled>
                                @if ($errors->has('kecamatan'))
                                <small class="text-danger">{{ $errors->first('kecamatan') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="desa">Nama Desa</label>
                                <input type="text" class="form-control" id="desa" name="desa" placeholder="Nama Desa" value="{{ old('desa') }}">
                                @if ($errors->has('desa'))
                                <small class="text-danger">{{ $errors->first('desa') }}</small>@endif
                            </div>
                        </div>
                    </div>
                    
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href=""><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

