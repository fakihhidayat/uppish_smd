@extends('layouts.admin')

@section('title')
Upload File Baru
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>
            
        <div class="col-md-12 grid-margin stretch-card">
                
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Upload File Baru</h4>
                <form class="forms-sample" action="{{ route('download.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Title File" value="{{ old('title') }}">
                            @if ($errors->has('title'))
                            <small class="text-danger">{{ $errors->first('title') }}</small>@endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="file">File</label>
                            <div class="input-group col-xs-12">
                                <input type="file" name="file" id="file" class="form-control file-upload-info" placeholder="File">
                            </div>
                            @if ($errors->has('file'))
                            <small class="text-danger">{{ $errors->first('file') }}</small>@endif
                        </div>
                    </div>
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="{{ route('download.index') }}"><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
