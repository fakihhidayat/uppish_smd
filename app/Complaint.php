<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $fillable = 
    [
        'nama', 'nik', 'id_desa', 'alamat', 'no_hp', 'topik_pengaduan', 'pesan'
    ];

    protected $hidden = 
    [
        'id', 'created_at', 'updated_at', 'status'
    ];
}
