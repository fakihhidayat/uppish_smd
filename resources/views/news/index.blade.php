@extends('layouts.admin')

@section('title')
    Info Terbaru  
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div>
            @endif
      </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="row">
                <div class="col-lg-10"><h4 class="card-title">Info Terbaru</h4></div>
                <div class="col-lg-2" align="right">
                    <a title="Tambah Berita | Info Terbaru" href="{{ route('news.create')}}">
                            <button type="button" class="btn btn-primary btn-fw" style="margin-right: 13px">
                                    <i class="mdi mdi-plus"></i>Tambah</button>
                    </a>
                </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped" id="datatable-news">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Judul</th>
                        <th>Kategori</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php $no = 1; ?>
                       @if(count($news) > 0)
                       @foreach ($news as $new)
                       <tr>
                            <input type="hidden" id="idNews" value="{{ $new->id }}">
                            <td>{{ $no++ }}</td>
                            <td>{{ $new->created_at }}</td>
                            <td>{{ $new->judul }}</td>
                            <td><button class="btn btn-outline-primary">{{ $new->newsCategory->nama_kategori }}</button></td>
                            <td><button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ $new->status }}
                              </button>
                              @if($new->status == 'published')
                              <div class="dropdown-menu">
                                <form action="{{ route('news.updateStatus', $new->id) }}" method="post">
                                  @method('PATCH')
                                @csrf
                                <input type="hidden" name="status" value="drafted">
                                <button type="submit"><a class="dropdown-item">
                                  <i class="mdi mdi-eye-off"></i> Draft-kan</a></button>
                                </form>
                              </div>
                              @else 
                              <div class="dropdown-menu">
                                  <form action="{{ route('news.updateStatus', $new->id) }}" method="post">
                                    @method('PATCH')
                                  @csrf
                                  <input type="hidden" name="status" value="published">
                                  <button type="submit"><a class="dropdown-item">
                                    <i class="mdi mdi-send"></i> Publish</a></button>
                                  </form>
                                </div>
                               @endif
                            </td>
                            <td>
                                <i class="mdi mdi-comment-text-outline mx-0"></i> {{ count($new->getComment) }} 
                                <br><i class="mdi mdi-eye mx-0"></i> 1.5k
                            </td>
                            </div>
                            <td><div class="btn-group" role="group" aria-label="Basic example">
                                  <button type="button" title="Ubah" class="btn btn-dark btn-sm"><a href="{{ route('news.edit', $new->id) }}" style="color: white"><i class="mdi mdi-pencil"></i></a></button>
                                  <form action="{{ route('news.destroy', $new->id) }}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" title="Hapus" class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                                  </form>
                                </div>
                            </td>
                        </tr>
                       @endforeach 
                       @else
                        <tr class="bg-secondary">
                          <td colspan="6" align="center">-- Tidak Ada Data --</td>
                        </tr>
                       @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

@endsection