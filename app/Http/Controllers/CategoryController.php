<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\News;
use App\Activity;
use App\NewsCategory;
use App\ActivityCategory;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request()->validate([
            'kategori_new' => 'required'
        ]);
             
            $cat = new NewsCategory([
                'nama_kategori' => $request->get('kategori_new'),
                'deskripsi' => $request->get('deskripsi')
            ]);
            $check = $cat->save();
            $arr = array('msg' => 'Something goes to wrong. Please try again lator', 'status' => false);
            if($check){ 
            $arr = array('msg' => 'Successfully submit form using ajax', 'status' => true);
            }
            return Response()->json($arr);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storeNew(Request $request)
    {
        $request()->validate([
            'kategori_new' => 'required'
        ]);
             
            $cat = new NewsCategory([
                'nama_kategori' => $request->get('kategori_new'),
                'deskripsi' => $request->get('deskripsi')
            ]);
            $cat->save();
            $arr = array('msg' => 'Something goes to wrong. Please try again lator', 'status' => false);
            if($check){ 
            $arr = array('msg' => 'Successfully submit form using ajax', 'status' => true);
            }
            return Response()->json($cat);
    }
}
