<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HumanResource;
use App\Employee;
use App\Job;
use App\SubDistrict;
use App\Village;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nik' => 'required',
            'nama' => 'required|string',
            'tgl' => 'required',
            'tmp' => 'required|string',
            'alamat' => 'required',
            'telp' => 'required',
            'email' => 'required|email',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $file       = Request()->file('image');
        $fileName   = $file->getClientOriginalName();
        Request()->file('image')->move(public_path('asset_general/images/employee'), $fileName);

        $employee = new Employee([
            'nik' => $request->get('nik'),
            'nama' => $request->get('nama'),
            'tgl_lahir' => $request->get('tgl'),
            'tmp_lahir' => $request->get('tmp'),
            'alamat' => $request->get('alamat'),
            'telp' => $request->get('telp'),
            'foto' => $fileName,
            'email' => $request->get('email')
        ]);

        $job = new Job([
            'id_humanresource' => $request->get('id_hr'),
            'nik_pegawai' => $request->get('nik'),
            'id_district' => $request->get('district'),
            'id_desa' => $request->get('village'),
        ]);

        if($employee->save() && $job->save())
        {
            return redirect()->route('humanresource.show', $request->get('id_hr'))->with('success','Karyawan berhasil ditambah !');
        }else
        {
            return back()->with('error','Karyawan gagal ditambah !');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employe = Job::where('id_humanresource', $id)->with(['getHumanResource', 'getEmployee'])->get();
        $hr = HumanResource::find($id);
        // if($id == 1)
        // {
        //     $kokab = Job::where('id_humanresource', $id)->with(['getHumanResource', 'getEmployee'])->first();
        //     return view('employee.showKordinator', compact('kokab'));
        // }

        if($id == 4 || $id == 5) {
        $jobs = Job::where('id_humanresource', $id)->with(['getDistrict','getVillage', 'getEmployee'])->get();
        $group = $jobs->groupBy(function ($job){
        return $job->getDistrict->nama_kecamatan;
        });

            if($id == 4) {
            return view('employee.listByDistrict', compact(['group','hr']));
            }
            if($id == 5) {
            return view('employee.listByDistrict', compact(['group','hr']));
            }

        } else {
            $hr = HumanResource::find($id);
            $otherEmployee = Job::where('id_humanresource', $id)->with(['getHumanResource', 'getEmployee'])->get();
            return view('employee.showOther', compact('otherEmployee', 'hr'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Job::where('nik_pegawai', $id)->with(['getHumanResource', 'getEmployee'])->first();
        $district = SubDistrict::orderBy('nama_kecamatan', 'asc')->get();
        return view('employee.formEdit', compact(['employee','kategori', 'district']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nik' => 'required',
            'nama' => 'required|string',
            'tgl' => 'required',
            'tmp' => 'required|string',
            'alamat' => 'required',
            'telp' => 'required',
            'email' => 'required|email'
        ]);

        $employee = Employee::where('nik', $id)->first();
        $employee->nik = $request->get('nik');
        $employee->nama = $request->get('nama');
        $employee->tgl_lahir = $request->get('tgl');
        $employee->tmp_lahir = $request->get('tmp');
        $employee->alamat = $request->get('alamat');
        $employee->telp = $request->get('telp');
        $employee->email = $request->get('email');
        $job = Job::where('nik_pegawai', $id)->first();
        $job->nik_pegawai = $request->get('nik');
        $job->id_district = $request->get('district');
        $job->id_desa = $request->get('village');

        $file       = Request()->file('image');

        if($file) {
            $fileName   = $file->getClientOriginalName();
            Request()->file('image')->move(public_path('asset_general/images/employee'), $fileName);
            $employee->foto = $fileName;
        }

        try {
            $employee->save();
            $job->save();
            return redirect()->route('humanresource.show', $request->get('id_hr'))->with('success','Karyawan berhasil diupdate !');
        }
        catch (\Exception $e) {
            return back()->with('error','Karyawan gagal diupdate !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::where('nik', $id)->delete();
        Job::where('nik_pegawai', $id)->delete();
        return back()->with('success', 'Karyawan berhasil dihapus!');
    }

    public function detail($id)
    {
        $detail = Employee::where('nik', $id)->first();
        return view('employee.detail', compact('detail'));
    }

    public function createNew($id)
    {
        $request = HumanResource::where('id', $id)->first();
        $districts = SubDistrict::orderBy('nama_kecamatan', 'asc')->pluck('nama_kecamatan', 'id');
        return view('employee.formCreate', compact(['request','districts']));
    }

    public function getVillage($id)
    {
        $village = Village::where('id_subdistrict', $id)->pluck("nama_desa","id");
        return json_encode($village);
    }
}
