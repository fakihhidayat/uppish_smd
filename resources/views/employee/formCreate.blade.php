@extends('layouts.admin')

@section('title')
    Tambah {{ $request->nama_sdm }} Baru
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>
        <div class="col-md-12 grid-margin stretch-card">

            <div class="card">
                <div class="card-body">
                <h4 class="card-title">SDM | Tambah {{ $request->nama_sdm }} Baru</h4>
                <form class="forms-sample" action="{{ route('employee.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <input type="text" hidden name="id_hr" value="{{ $request->id }}">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nik">NIK (Nomor Induk Kependudukan)</label>
                                <input type="text" class="form-control" id="nik" name="nik" placeholder="NIK" value="{{ old('nik') }}" onkeypress="return hanyaAngka(event)" maxlength="16">
                                @if ($errors->has('nik'))
                                <small class="text-danger">{{ $errors->first('nik') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="nama">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" value="{{ old('nama') }}">
                                    @if ($errors->has('nik'))
                                    <small class="text-danger">{{ $errors->first('nama') }}</small>@endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tgl">Tanggal Lahir</label>
                                <input type="text" class="form-control" id="tgl" name="tgl" placeholder="Tanggal Lahir" value="{{ old('tgl') }}">
                                @if ($errors->has('tgl'))
                                <small class="text-danger">{{ $errors->first('tgl') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tmp">Tempat Lahir</label>
                                <input type="text" class="form-control" id="tmp" name="tmp" placeholder="Tempat Lahir" value="{{ old('tmp') }}">
                                @if ($errors->has('tmp'))
                                <small class="text-danger">{{ $errors->first('tmp') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" value="{{ old('alamat') }}">
                                    @if ($errors->has('alamat'))
                                    <small class="text-danger">{{ $errors->first('alamat') }}</small>@endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="telp">Telp/HP</label>
                                    <input type="text" class="form-control" id="telp" name="telp" placeholder="Nomor Telepon/HP" value="{{ old('telp') }}" onkeypress="return hanyaAngka(event)">
                                    @if ($errors->has('telp'))
                                    <small class="text-danger">{{ $errors->first('telp') }}</small>@endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="E-mail" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                    <small class="text-danger">{{ $errors->first('email') }}</small>@endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Gambar/Foto</label>
                                <div class="input-group col-xs-12">
                                    <input type="file" name="image" id="profile-img" class="form-control file-upload-info" placeholder="Gambar/Foto Info" value="{{ old('image') }}">
                                </div>
                                @if ($errors->has('image'))
                                <small class="text-danger">{{ $errors->first('image') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Preview</label><br>
                                <img src="{{ asset('asset_general/images/news/preview.png') }}" id="profile-img-tag" width="100px" height="100px" />
                            </div>
                        </div>
                    </div>

                    @if($request->id == 4 || $request->id == 5)
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="district">Wilayah Kerja</label>
                                <select name="district" id="district" class="form-control" required>
                                    <option value="">Pilih Kecamatan</option>
                                    @foreach ($districts as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @if($request->id == 5)

                        @else
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="village"></label>
                                <select name="village" id="village" class="form-control">
                                    <option value="">Desa/Kelurahan (Pilih Kecamatan Terlebih Dahulu)</option>
                                </select>
                            </div>
                        </div>
                        @endif
                    </div>
                    @endif

                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="{{ route('news.index') }}"><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
</script>

<script>
        function hanyaAngka(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
          return true;
        }
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function()
        {
            jQuery('select[name="district"]').on('change', function(){
                var districtID = jQuery(this).val();
                if(districtID)
                {
                    jQuery.ajax({
                        url : '/uppkh_smd/public/employee/getVillage/' +districtID,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            jQuery('select[name="village"]').empty();
                            jQuery.each(data, function(key,value){
                                $('select[name="village"]').append('<option value="'+ key +'">'+ value +'</option>');
                            });
                        }
                    });
                } else
                {
                    $('select[name="village"]').empty();
                }
            });
        });
    </script>

@endpush
