
@extends('layouts.frontend')

@section('title')
    Kecamatan {{ $subdistrict->nama_kecamatan }}
@endsection

@section('content')
<br>
@if(auth()->user()->admin==1)
<div class="container">

        <div class="row">
            <div class="col-md-8 col-lg-9 tr-sticky">
                <h1 class="section-title title">{{ $subdistrict->nama_kecamatan }}</h1>
                {{-- {{$chart}} --}}
                <div class="contact-us theiaStickySidebar">
                    <div class="map-section">
                        <div id="gmap"></div>
                    </div>
                    <div class="section world-news">
                        <h1 class="section-title title">Profil</h1>
                        <div class="post">
                            <div class="post-content">
                                <div class="entry-content">
                                    <p>{!! nl2br(e($subdistrict->profil)) !!}</p>
                                </div>
                            </div>
                        </div><!--/post-->
                    </div><!--/.section-->
                    @if(isset($chart))
                    <div class="section world-news">
                            <h1 class="section-title title">Penerima Bantuan PKH</h1>
                            <div class="post">
                                <div class="post-content">
                                    <div class="entry-content">
                                        <canvas id="oilChart" width="400" height="200"></canvas>
                                    <center><h3>Total Penerima: {{$chart->jumlah}} </h3></center>
                                    </div>
                                </div>
                            </div><!--/post-->
                        </div><!--/.section-->
                        @endif
                </div><!-- contact-us -->
            </div>
            @include('layouts.frontend_sidebar')
        </div><!-- row -->
    </div><!--/.container-->
@endif
    {{-- {!! Charts::scripts() !!} --}}
{{-- {!! $chart->script() !!} --}}
@endsection

@push('scripts')
<script>
    (function(){

        var map;

        map = new GMaps({
            el: '#gmap',
            lat: {{ $subdistrict->lat }},
            lng: {{ $subdistrict->long }},
            scrollwheel:false,
            zoom: 15,
            zoomControl : true,
            panControl : false,
            streetViewControl : true,
            mapTypeControl: true,
            overviewMapControl: true,
            clickable: true
        });

        var image = '';
        map.addMarker({
            lat: {{ $subdistrict->lat }},
            lng: {{ $subdistrict->long }},
            icon: image,
            animation: google.maps.Animation.DROP,
            verticalAlign: 'bottom',
            horizontalAlign: 'center',
            backgroundColor: '#d3cfcf',
             infoWindow: {
                content: '<div class="map-info"><address>Kecamatan {{$subdistrict->nama_kecamatan}}</address></div>',
                borderColor: 'red',
            }
        });

        var styles = [

            {
              "featureType": "road",
              "stylers": [
                { "color": "#c1c1c1" }
              ]
              },{
              "featureType": "water",
              "stylers": [
                { "color": "#f1f1f1" }
              ]
              },{
              "featureType": "landscape",
              "stylers": [
                { "color": "#e3e3e3" }
              ]
              },{
              "elementType": "labels.text.fill",
              "stylers": [
                { "color": "#808080" }
              ]
              },{
              "featureType": "poi",
              "stylers": [
                { "color": "#dddddd" }
              ]
              },{
              "elementType": "labels.text",
              "stylers": [
                { "saturation": 1 },
                { "weight": 0.1 },
                { "color": "#7f8080" }
              ]
            }

        ];

    map.addStyle({
            styledMapName:"Styled Map",
            styles: styles,
            mapTypeId: "map_style"
        });

        map.setStyle("map_style");
    }());

    var oilCanvas = document.getElementById("oilChart");

        Chart.defaults.global.defaultFontFamily = "Lato";
        Chart.defaults.global.defaultFontSize = 18;

        var oilData = {
            labels: [
                "Bumil",
                "Balita",
                "Apras",
                "SD",
                "SMP",
                "SMA",
                "Lansia",
                "Disabilitas"
            ],
            datasets: [
                {
                    data: ["{{$chart->bumil}}", "{{$chart->balita}}","{{$chart->apras}}", "{{$chart->sd}}", "{{$chart->smp}}","{{$chart->sma}}","{{$chart->lansia}}","{{$chart->disabilitas}}"],
                    backgroundColor: [
                        "#FF6384",
                        "#63FF84",
                        "#84FF63",
                        "#8463FF",
                        "#6384FF",
                        "#63FF84",
                        "#84FF63",
                        "#8463FF"
                    ]
                }]
        };

        var pieChart = new Chart(oilCanvas, {
        type: 'pie',
        data: oilData
        });
</script>
@endpush
