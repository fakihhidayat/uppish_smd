<?php

use App\Product;
use App\NewsCategory;
use App\NewsComment;
use App\News;

$products = Product::orderBy('created_at', 'desc')->where('status', 'sale')->paginate(3);
$categories = NewsCategory::All();
$comments = NewsComment::orderBy('created_at', 'desc')->with('news')->limit(5)->get();
$news =  News::orderBy('created_at', 'desc')->paginate(3);

?>

<div class="col-md-4 col-lg-3 tr-sticky">
        <div id="sitebar" class="theiaStickySidebar">
            <div class="widget">
                @yield('spasi')
                <h1 class="section-title title">Produk Ekonomi</h1>
                <ul class="post-list">
                    @if(count($products) > 0)
                    @foreach($products as $product)
                    <li>
                        <div class="post small-post">
                            <div class="entry-header">
                                <div class="entry-thumbnail">
                                    <img class="img-fluid" height="95" width="95" src="{{ asset('asset_general/images/product/'.$product->foto.'') }}" alt="" />
                                </div>
                            </div>
                            <div class="post-content">
                                <div class="video-catagory"><a href="{{ route('product.showAll') }}">{{ $product->nama_produk }}</a></div>
                                <h2 class="entry-title">
                                    Rp {{ $product->harga }}/{{ $product->jual_per }}
                                </h2>
                                <h2 class="entry-title">
                                    {{ $product->deskripsi }}
                                </h2>
                            </div>
                        </div><!--/post-->
                    </li>
                    @endforeach
                    @else
                    <li>
                        <div class="post small-post">
                            <div class="post-content">
                                <div class="video-catagory"><a href="">-- Belum ada data yang tersedia --</a></div>
                            </div>
                        </div><!--/post-->
                    </li>
                    @endif
                </ul>
            </div><!--/#widget-->



            <div class="widget meta-widget">
                <h1 class="section-title title">Berita</h1>
                <div class="meta-tab">
                    <div class="tab-content">
                        <ul class="authors-post">
                            @if(count($news) > 0)
                            @foreach($news as $n)
                            <li>
                                <div class="post small-post">
                                    <div class="post-content">
                                        <div class="entry-meta">
                                            <ul class="list-inline">
                                                <li class="post-author"><a href="index5.html#">Admin,</a></li>
                                                <li class="publish-date"><a href="index5.html#">{{ $n->created_at }} </a></li>
                                            </ul>
                                        </div>
                                        <h2 class="entry-title">
                                            <a href="{{ route('news.showById', $n->id) }}">{{$n->judul}}</a>
                                        </h2>
                                    </div>
                                </div><!--/post-->
                            </li>
                            @endforeach
                            @else
                            <li>
                                <div class="post small-post">
                                    <div class="post-content">
                                        <div class="video-catagory"><a href="">-- Belum ada data yang tersedia --</a></div>
                                    </div>
                                </div><!--/post-->
                            </li>
                            @endif

                        </ul>
                    </div>
                </div>
            </div><!--/#widget-->


        </div><!--/#sitebar-->
    </div>
