<?php
 use App\HumanResource;
 use App\SubDistrict;
 use App\ActivityCategory;
 use App\Contact;
 use App\Download;


 $humanresources = HumanResource::All();
 $subdistricts = SubDistrict::orderBy('nama_kecamatan', 'asc')->get();
 $activitycategories = ActivityCategory::All();
 $contact = Contact::All()->first();


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="PKH Sumedang">
		<meta name="csrf-token" content="{{ csrf_token() }}">
	<!--title-->
    <title>@yield('title') @if(isset($contact))| {{ $contact->nama_lembaga }}@endif</title>

	<!--CSS-->
  <link href="{{ asset('asset_general/css/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link href="{{ asset('asset_general/css/magnific-popup.css') }}" rel="stylesheet">
	<link href="{{ asset('asset_general/css/owl.carousel.css') }}" rel="stylesheet">
	<link href="{{ asset('asset_general/css/subscribe-better.css') }}" rel="stylesheet">
	<link href="{{ asset('asset_general/css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('asset_general/css/presets/preset1.css') }}" rel="stylesheet">
	<link href="{{ asset('asset_general/css/responsive.css') }}" rel="stylesheet">



	<!--Google Fonts-->
	<link href="{{ asset('asset_general/css/font.css.css') }}" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
	    <script src="{{ asset('asset_general/js/html5shiv.js') }}"></script>
	    <script src="{{ asset('asset_general/js/respond.min.js') }}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ asset('asset_general/images/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('asset_general/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('asset_general/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('asset_general/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('asset_general/images/ico/apple-touch-icon-57-precomposed.png') }}">
    <link href="{{ asset('asset_general/chartjs/Chart.min.css') }}" rel="stylesheet">
		{!! Charts::styles() !!}
</head><!--/head-->
<body>
	<div id="main-wrapper" class="homepage-five">
			<header id="navigation">
					<div class="navbar navbar-expand-lg" role="banner">
						<div class="container">
							<a class="secondary-logo" href="index.html">
								<img class="img-fluid" src="{{ asset('asset_general/images/presets/preset1/logo.png') }}" alt="logo">
							</a>
						</div>
						{{-- <div class="topbar">
							<div class="container">
								<div id="topbar" class="navbar-header">
									<a class="navbar-brand" href="index.html">
										<img class="main-logo img-fluid" src="{{ asset('asset_general/images/presets/preset1/logo.png') }}" alt="logo">
									</a>
									<div id="topbar-right">

										<div id="date-time"></div>
										<div id="weather"></div>
									</div>
								</div>
							</div>

						</div>  --}}
						<div class="container">
								<div class="section add inner-add">
										<a href="{{ route('/') }}"><img class="img-fluid" src="{{ asset('asset_general/images/header-pkh.jpg') }}" alt="" /></a>
									</div><!--/.section-->
						</div>

						<div id="menubar" class="container">
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainmenu" aria-controls="mainmenu" aria-expanded="false" aria-label="Toggle navigation">
								<span class="navbar-toggler-icon"><i class="fa fa-align-justify"></i></span>
							</button>
							<a class="navbar-brand d-lg-none" href="index.html">
								<img class="main-logo img-fluid" src="{{ asset('asset_general/images/presets/preset1/logo.png') }}" alt="logo">
							</a>
							<nav id="mainmenu" class="navbar-left collapse navbar-collapse">
								<ul class="nav navbar-nav">
									<li class="home"><a href="{{ route('/') }}">Beranda</a></li>
									<li class="business dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Profil +</a>
										<ul class="dropdown-menu">
											<li><a href="{{ route('profile.pkh') }}">PKH</a></li>
											<li><a href="{{ route('profile.visimisi') }}">Visi & Misi</a></li>
											<li><a href="{{ route('profile.tugasfungsi') }}">Tugas & Fungsi</a></li>
										</ul>
									</li>
									<li class="politics dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">SDM +</a>
										<ul class="dropdown-menu">
											@foreach ($humanresources as $humanresource)
											<li><a href="{{ route('employee.show', $humanresource->id) }}">{{ $humanresource['nama_sdm'] }}</a></li>
											@endforeach
										</ul>
									</li>
									<li class="sports dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Kecamatan +</a>
										<ul class="dropdown-menu">
											@foreach ($subdistricts as $subdistrict)
												<li><a href="{{ route('subdistrict.show', $subdistrict->id) }}">{{ $subdistrict['nama_kecamatan'] }}</a></li>
											@endforeach

										</ul>
									</li>
									<li class="world dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Kegiatan +</a>
										<ul class="dropdown-menu">
											@foreach ($activitycategories as $category)
												<li><a href="{{ route('activity.show', $category->id) }}">{{ $category['kat_kegiatan'] }}</a></li>
											@endforeach
										</ul>
									</li>
									<li class="environment dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Berita +</a>
										<ul class="dropdown-menu">
											<li><a href="{{ route('news.showAll') }}">Info Terbaru</a></li>
											<li><a href="">Catatan Pendamping</a></li>
										</ul>
									</li>
									<li class="health dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pemberdayaan +</a>
										<ul class="dropdown-menu">
											<li><a href="{{ route('product.showAll') }}">Ekonomi</a></li>
										</ul>
									</li>
									<li class="entertainment"><a href="{{ route('complaint.formComplaint') }}">Sistem Pengaduan</a></li>
									<li class="politics dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Laporan +</a>
										<ul class="dropdown-menu">
											<li><a href="">Laporan Pendamping</a></li>
											<li><a href="">Laporan Graduasi</a></li>
										</ul>
									</li>
									<li class="lifestyle"><a href="{{ route('download.showDownload') }}">Download</a></li>
									<li class="moore"><a href="{{ route('contact.map') }}">Kontak</a></li>
								</ul>
							</nav>
							<div class="searchNlogin">
									<ul>
										<li class="search-icon"><i class="fa fa-search"></i></li>
										<li class="user-panel"><a href="{{ route('login') }}"><i class="fa fa-user"></i></a></li>
									</ul>
									<div class="search">
										<form role="form">
											<input type="text" class="search-form" autocomplete="off" placeholder="Type & Press Enter">
										</form>
									</div> <!--/.search-->
								</div><!-- searchNlogin -->
						</div>
					</div>
				</header><!--/#navigation-->



	<!-- Home -->

	@yield('content')

	<!-- Footer -->

	<div class="footer-widget">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="widget">
						<h1 class="section-title title">Kontak Kami</h1>
						@if(isset($contact))
						<p>{{ $contact->nama_lembaga }}</p>
						<address>
							<p>Alamat : {{ $contact->alamat }}</p>
							<p>Telepon : {{ $contact->no_telp }}</p>
							<p>Email: {{ $contact->email }}</a></p>
						</address>
						@else
						<p>-- Belum ada data yang tersedia --</p>
						@endif
					</div>
				</div>
				<div class="col-sm-4">
					<div class="widget">
						<h1 class="section-title title">Sosial Media</h1>
						<ul class="list-inline social-icons">
							<li><a href="index5.html#"><i class="fab fa-facebook"></i></a></li>
							<li><a href="index5.html#"><i class="fab fa-twitter"></i></a></li>
							<li><a href="index5.html#"><i class="fab fa-google-plus"></i></a></li>
							<li><a href="index5.html#"><i class="fab fa-youtube"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="widget news-letter">
						<h1 class="section-title title">Berlangganan</h1>
						<p>Dapatkan informasi terbaru dari @if(isset($contact)){{ $contact->nama_lembaga }}.@else ______ @endif</p>
						<p>Gabung bersama 1.435.065 pelanggan lainnya</p>
						<form action="index5.html#" method="post" id="subscribe-form" name="subscribe-form">
							<input type="text" placeholder="E-mail kamu" name="email">
							<button type="submit" name="subscribe" id="subscribe" >Langganan</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div><!--/.footer-widget-->

	<footer id="footer">
		<div class="footer-top">
			<div class="container text-center">
				<div class="logo-icon"><img class="img-fluid" src="{{ asset('asset_general/images/presets/preset1/PKH.png') }}" alt="" /></div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container text-center">
				<p><a href="#">@if(isset($contact)) {{ $contact->nama_lembaga }} @endif</a>&copy; 2019 </p>
			</div>
		</div>
	</footer>
</div><!--/#main-wrapper-->


<!--/#scripts-->
<script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
<script src="{{ asset('js/newjs.js') }}"></script>
<script data-cfasync="false" src="{{ asset('asset_general/js/email-decode.min.js') }}"></script><script src="{{ asset('asset_general/js/jquery.js') }}"></script>
<script src="{{ asset('asset_general/js/popper.min.js') }}"></script>
<script src="{{ asset('asset_general/js/bootstrap.min.js') }}"></script>
<script src="https://maps.google.com/maps/api/js?sensor=true"></script>
<script src="https://demo.themeregion.com/newspress/js/gmaps.js"></script>
<script src="{{ asset('asset_general/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('asset_general/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('asset_general/js/moment.min.js') }}"></script>
<script src="{{ asset('asset_general/js/jquery.simpleWeather.min.js') }}"></script>
<script src="{{ asset('asset_general/js/jquery.sticky-kit.min.js') }}"></script>
<script src="{{ asset('asset_general/js/jquery.easy-ticker.min.js') }}"></script>
<script src="{{ asset('asset_general/js/jquery.subscribe-better.min.js') }}"></script>
<script src="{{ asset('asset_general/js/theia-sticky-sidebar.min.js') }}"></script>
<script src="{{ asset('asset_general/js/main.js') }}"></script>
<script src="{{ asset('asset_general/js/switcher.js') }}"></script>
<script src="{{ asset('asset_general/chartjs/Chart.min.js') }}"></script>



@stack('scripts')
@include('layouts._modal')
@yield('modal')

</body>
</html>
