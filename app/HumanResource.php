<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Job;

class HumanResource extends Model
{
    public function getEmployees()
    {
        return $this->hasMany(Job::class, 'id_humanresource', 'id');
    }
}
