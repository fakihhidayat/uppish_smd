@extends('layouts.admin')

@section('title')
    Video 
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div>
            @endif
      </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="row">
                <div class="col-lg-10"><h4 class="card-title">Video</h4></div>
                <div class="col-lg-2" align="right">
                    <a href="{{ route('video.create')}}">
                        <button type="button" class="btn btn-primary btn-fw" style="margin-right: 13px">
                        <i class="mdi mdi-plus"></i>Tambah</button>
                    </a>
                </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped" id="datatable-news">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Preview</th>
                        <th>Tanggal</th>
                        <th>Judul</th>
                        <th>Kategori</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php $no = 1; ?>
                       @if(count($video) > 0)
                       @foreach ($video as $v)
                       <tr>
                            <td>{{ $no++ }}</td>
                            <td><div class="entry-thumbnail embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $v->url_youtube }}" frameborder="0" allowfullscreen></iframe>
                            </div></td>
                            <td>{{ $v->created_at }}</td>
                            <td>{{ $v->judul }}</td>
                            <td><button class="btn btn-outline-primary">{{ $v->getVideoCategory->nama_kategori }}</button></td>
                            </div>
                            <td><div class="btn-group" role="group" aria-label="Basic example">
                                  <button type="button" title="Ubah" class="btn btn-dark btn-sm"><a href="{{ route('video.edit', $v->id) }}" style="color: white"><i class="mdi mdi-pencil"></i></a></button>
                                  <form action="{{ route('video.destroy', $v->id) }}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" title="Hapus" class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                                  </form>
                                </div>
                            </td>
                        </tr>
                       @endforeach 
                       @else
                        <tr class="bg-secondary">
                          <td colspan="6" align="center">-- Tidak Ada Data --</td>
                        </tr>
                       @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

@endsection