@extends('layouts.admin')

@section('title')
    Tambah Kecamatan Baru
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>
        
            
        <div class="col-md-12 grid-margin stretch-card">
                
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Kecamatan | Tambah Kecamatan Baru</h4>
                <form class="forms-sample" action="{{ route('subdistrict.store') }}" method="POST">
                    {{ csrf_field() }}
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kecamatan">Nama Kecamatan</label>
                                <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="Nama Kecamatan" value="{{ old('kecamatan') }}">
                                @if ($errors->has('kecamatan'))
                                <small class="text-danger">{{ $errors->first('kecamatan') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="lat">Kordinat Latitude</label>
                                <input type="text" class="form-control" id="lat" name="lat" placeholder="Latitude" value="{{ old('lat') }}">
                                @if ($errors->has('lat'))
                                <small class="text-danger">{{ $errors->first('lat') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="long">Kordinat Longitude</label>
                                <input type="text" class="form-control" id="long" name="long" placeholder="Longitude" value="{{ old('long') }}">
                                @if ($errors->has('long'))
                                <small class="text-danger">{{ $errors->first('long') }}</small>@endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="profil">Profil</label>
                        <textarea class="form-control" id="profil" rows="15" name="profil">{{ old('profil')}}</textarea>
                        @if ($errors->has('profil'))
                        <small class="text-danger">{{ $errors->first('profil') }}</small>@endif
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href=""><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

