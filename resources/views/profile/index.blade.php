@extends('layouts.admin')

@section('title')
    Profil Lembaga 
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div>
            @endif
      </div>
      <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
          <div class="row">
            <div class="col-lg-10"><h4 class="card-title">Selayang Pandang</h4></div>
            <div class="col-lg-2" align="right">
              @if(isset($sepa))
              <a href="{{ route('profile.edit', $sepa->id )}}">
                <button type="button" class="btn btn-primary btn-fw" style="margin-left: -60px">
                <i class="mdi mdi-pencil"></i>Edit</button>
              </a>
              @else
              <a href="{{ route('profile.createNew', 'Selayang Pandang')}}">
                  <button type="button" class="btn btn-primary btn-fw" style="margin-left: -60px">
                  <i class="mdi mdi-plus"></i>Isi</button>
              </a>
              @endif
            </div>
          </div>
          </div>
          @if(isset($sepa))
          <div class="card-body">
            <blockquote class="blockquote blockquote-primary">
              <p>{!! nl2br(e($sepa->isi)) !!}</p>
            </blockquote>
          </div>
          @else
          <div class="card-body">
          <blockquote class="blockquote blockquote-primary">
            <footer class="blockquote-footer">
              <cite title="Source Title">Belum Ada Data</cite>
            </footer>
          </blockquote>
          </div>
          @endif
        </div>
      </div>
      <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="row">
                <div class="col-lg-10"><h4 class="card-title">Profile Lengkap</h4></div>
                <div class="col-lg-2" align="right">
                  @if(isset($profiles))
                  <a href="{{ route('profile.edit', $profiles->id )}}">
                    <button type="button" class="btn btn-primary btn-fw" style="margin-left: -60px">
                    <i class="mdi mdi-pencil"></i>Edit</button>
                  </a>
                  @else
                  <a href="{{ route('profile.createNew', 'Profil')}}">
                      <button type="button" class="btn btn-primary btn-fw" style="margin-left: -60px">
                      <i class="mdi mdi-plus"></i>Isi</button>
                  </a>
                  @endif
                </div>
              </div>
          </div>
          @if(isset($profiles))
          <div class="card-body">
            <blockquote class="blockquote blockquote-primary">
              <p>{!! nl2br(e($profiles->isi)) !!}</p>
            </blockquote>
          </div>
          @else
          <div class="card-body">
          <blockquote class="blockquote blockquote-primary">
            <footer class="blockquote-footer">
              <cite title="Source Title">Belum Ada Data</cite>
            </footer>
          </blockquote>
          </div>
          @endif
        </div>
      </div>
      <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="row">
                <div class="col-lg-10"><h4 class="card-title">Visi & Misi</h4></div>
                <div class="col-lg-2" align="right">
                  @if(isset($visions) && isset($missions))
                  <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-primary btn-sm" style="margin-left: -150px"><a href="{{ route('profile.edit', $visions->id) }}" style="color: white"><i class="mdi mdi-pencil"></i> Edit Visi</a></button>
                    <button type="button" class="btn btn-primary btn-sm"><a href="{{ route('profile.edit', $missions->id) }}" style="color: white"><i class="mdi mdi-pencil"></i> Edit Misi</a></button>
                  </div>
                  @endif
                  @if(!isset($visions) && !isset($missions))
                  <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-primary btn-sm" style="margin-left: -150px"><a href="{{ route('profile.createNew', 'Visi') }}" style="color: white"><i class="mdi mdi-plus"></i> Isi Visi</a></button>
                    <button type="button" class="btn btn-primary btn-sm"><a href="{{ route('profile.createNew', 'Misi') }}" style="color: white"><i class="mdi mdi-plus"></i> Isi Misi</a></button>
                  </div>
                  @endif
                  @if(isset($visions) && !isset($missions))
                  <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-primary btn-sm" style="margin-left: -150px"><a href="{{ route('profile.edit', $visions->id) }}" style="color: white"><i class="mdi mdi-pencil"></i> Edit Visi</a></button>
                    <button type="button" class="btn btn-primary btn-sm"><a href="{{ route('profile.createNew', 'Misi') }}" style="color: white"><i class="mdi mdi-plus"></i> Isi Misi</a></button>
                  </div>
                  @endif
                  @if(isset($missions) && !isset($visions))
                  <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-primary btn-sm" style="margin-left: -150px"><a href="{{ route('profile.create', 'Visi') }}" style="color: white"><i class="mdi mdi-plus"></i> Isi Visi</a></button>
                    <button type="button" class="btn btn-primary btn-sm"><a href="{{ route('profile.edit', $missions->id) }}" style="color: white"><i class="mdi mdi-pencil"></i> Edit Misi</a></button>
                  </div>
                  @endif
                </div>
              </div>
            @if(isset($visions) || isset($missions))
            @if(isset($visions))
            <ul class="list-star">
              <li>Visi</li>
            </ul>
            <p class="card-description">{!! nl2br(e($visions->isi)) !!}</p>
            @endif
            @if(isset($missions))
            <ul class="list-star">
                <li>Misi</li>
            </ul>
            <p class="card-description">{!! nl2br(e($missions->isi)) !!}</p>
            @endif
            @else
            <blockquote class="blockquote blockquote-primary">
              <footer class="blockquote-footer">
                <cite title="Source Title">Belum Ada Data</cite>
              </footer>
            </blockquote>
            @endif
          </div>
        </div>
      </div>
      <div class="col-md-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <div class="row">
                  <div class="col-lg-10"><h4 class="card-title">Tugas & Fungsi</h4></div>
                  <div class="col-lg-2" align="right">
                    @if(isset($tf))
                    <a href="{{ route('profile.edit', $tf->id )}}">
                      <button type="button" class="btn btn-primary btn-fw" style="margin-left: -60px">
                      <i class="mdi mdi-pencil"></i>Edit</button>
                    </a>
                    @else
                    <a href="{{ route('profile.createNew', 'Tugas dan Fungsi')}}">
                        <button type="button" class="btn btn-primary btn-fw" style="margin-left: -60px">
                        <i class="mdi mdi-plus"></i>Isi</button>
                    </a>
                    @endif
                  </div>
                </div>
              @if(isset($tf))
              <ul class="list-star">
                <li>Tugas & Fungsi</li>
              </ul>
              <p class="card-description">{!! nl2br(e($tf->isi)) !!}</p>
              @else
              <blockquote class="blockquote blockquote-primary">
                <footer class="blockquote-footer">
                  <cite title="Source Title">Belum Ada Data</cite>
                </footer>
              </blockquote>
              @endif
            </div>
          </div>
        </div>
    </div>
</div>

@endsection