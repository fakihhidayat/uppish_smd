@extends('layouts.admin')

@section('title')
    Tambah Peserta PKH
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>            
        <div class="col-md-12 grid-margin stretch-card">
                
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Tambah | Peserta PKH</h4>
                <form class="forms-sample" action="{{ route('participant.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nopkh">Nomor PKH</label>
                                <input type="text" class="form-control" id="nopkh" name="nopkh" placeholder="Nomor PKH" value="{{ old('nopkh') }}" onkeypress="return hanyaAngka(event)" maxlength="15">
                                @if ($errors->has('nopkh'))
                                <small class="text-danger">{{ $errors->first('nopkh') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Nama Pengurus</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Pengurus" value="{{ old('nama') }}">
                                @if ($errors->has('nama'))
                                <small class="text-danger">{{ $errors->first('nama') }}</small>@endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="district">Kecamatan</label>
                                <select name="district" id="district" class="form-control">
                                    <option value="">--Pilih Kecamatan--</option> 
                                    @foreach ($districts as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option> 
                                    @endforeach
                                </select>   
                                @if ($errors->has('district'))
                                <small class="text-danger">{{ $errors->first('district') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="village">Desa/Kelurahan</label>
                                <select name="village" id="village" class="form-control">
                                    <option value="">--Pilih Desa/Kelurahan--</option> 
                                </select> 
                                @if ($errors->has('village'))
                                <small class="text-danger">{{ $errors->first('village') }}</small>@endif  
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat Lengkap" value="{{ old('alamat') }}">
                                @if ($errors->has('alamat'))
                                <small class="text-danger">{{ $errors->first('alamat') }}</small>@endif
                            </div>
                        </div>
                    </div>
                    
                    <hr>
                    <div class="row">
                        <div class="col-md-2">
                            
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="bumil">Bumil</label>
                                <input type="number" class="form-control" id="number" min="0" name="bumil" value="{{ old('bumil', 0) }}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="balita">Balita</label>
                                <input type="number" class="form-control" id="number" name="balita" value="{{ old('balita', 0) }}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="apras">APRAS</label>
                                <input type="number" class="form-control" id="number" name="apras" value="{{ old('apras', 0) }}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="sd">SD</label>
                                <input type="number" class="form-control" id="number" name="sd" value="{{ old('sd', 0) }}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="smp">SMP</label>
                                <input type="number" class="form-control" id="number" name="smp" value="{{ old('smp', 0) }}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="sma">SMA</label>
                                <input type="number" class="form-control" id="number" name="sma" value="{{ old('sma', 0) }}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="lansia">Lansia</label>
                                <input type="number" class="form-control" id="number" name="lansia" value="{{ old('lansia', 0) }}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="disabilitas">Disabilitas</label>
                                <input type="number" class="form-control" id="number" name="disabilitas" value="{{ old('disabilitas', 0) }}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <input type="hidden" id="total" name="total">
                        </div>
                    </div>
                    
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="{{ route('participant.index') }}"><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script>
    $('.form-group').on('input','#number', function(){
        var totalSum = 0;
        $('.form-group #number').each(function(){
            var inputVal = $(this).val();
            if($.isNumeric(inputVal)){
                totalSum += parseFloat(inputVal);
            }
        });
        $('#total').val(totalSum);
    });

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
        return true;
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function()
    {
        jQuery('select[name="district"]').on('change', function(){
            var districtID = jQuery(this).val();
            if(districtID)
            {
                jQuery.ajax({
                    url : 'getVillage/' +districtID,
                    type : "GET",
                    dataType : "json",
                    success:function(data)
                    {
                        console.log(data);
                        jQuery('select[name="village"]').empty();
                        jQuery.each(data, function(key,value){
                            $('select[name="village"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                    }
                });
            } else 
            {
                $('select[name="village"]').empty();
            }
        });
    });

</script>

@endpush