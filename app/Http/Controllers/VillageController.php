<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Village;
use App\SubDistrict;

class VillageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function createNew($id)
    {
        $request = SubDistrict::where('id', $id)->first();
        return view('village.formCreate', compact('request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'desa' => 'required'
        ]);

        $village = new Village;
        $village->id_subdistrict = $request->get('district');
        $village->nama_desa = $request->get('desa');

        try {
            $village->save();
            return redirect()->route('subdistrict.index')->with('success','Desa berhasil ditambah !');
        }
        catch (\Exception $e) {
            return back()->with('error','Desa gagal ditambah !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $village = Village::where('id', $id)->with('subdisctrict')->first();
        return view('village.formEdit', compact('village'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'district' => 'required',
            'desa' => 'required'
        ]);

        $village = Village::find($id);
        $village->id_subdistrict = $request->get('district');
        $village->nama_desa = $request->get('desa');

        try {
            $village->save();
            return redirect()->route('subdistrict.index')->with('success','Desa berhasil diupdate !');
        }
        catch (\Exception $e) {
            return back()->with('error','Desa gagal diupdate !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Village::destroy($id);
        return back()->with('success', 'Desa berhasil dihapus!');
    }
}
