
@extends('layouts.frontend')

@section('title')
    {{ $category->kat_kegiatan }}
@endsection

@section('spasi')
    <br>
@endsection

@section('content')
<div class="container">
    <div class="section">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div id="site-content" class="site-content">
                        <div class="row">
                            <div class="col">
                                <div class="left-content">
                                    <div class="details-news">
                                        <div class="post">
                                            <div class="entry-header">
                                                <div class="post-content">
                                                    <h2 class="entry-title">
                                                        {{ $activity->judul }}
                                                    </h2>
                                                    <div class="entry-meta">
                                                        <ul class="list-inline">
                                                            <li class="posted-by"><i class="fa fa-user"></i> by {{ $activity->admin_id }}</li>
                                                            <li class="publish-date"><i class="fa fa-clock"></i> {{ $activity->created_at }} </li>
                                                            <li class="views"><i class="fa fa-eye"></i>15k</li>
                                                            <li class="loves"><i class="fas fa-map-marker-alt"></i>{{ $activity->lok_kegiatan }}</li>
                                                            <li class="comments"><i class="fa fa-comment"></i>{{ count($publish) }}</li>
                                                        </ul>
                                                    </div>
                                                    <hr>
                                                </div>
                                                <div class="entry-thumbnail" style="margin-top: -40px">
                                                    <img class="img-fluid" height="497" width="871" src="{{ asset('asset_general/images/activity/'.$activity->foto.'') }}" alt="" />
                                                </div>
                                            </div>
                                            <div class="post-content">
                                                <div class="entry-content">
                                                    <p>{!! $activity->uraian !!}</p>

														<div class="row post-inner-image">
                                                                <div class="col-sm-4">
                                                                    <img class="img-fluid" src="https://demo.themeregion.com/newspress/images/post/inner1.jpg" alt="" />
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <img class="img-fluid" src="https://demo.themeregion.com/newspress/images/post/inner2.jpg" alt="" />
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <img class="img-fluid" src="https://demo.themeregion.com/newspress/images/post/inner3.jpg" alt="" />
                                                                </div>
                                                            </div><!-- post-inner-image -->
                                                    <ul class="list-inline share-link">
                                                        <li><a href="news-details.html#"><img src="https://demo.themeregion.com/newspress/images/others/s1.png" alt="" /></a></li>
                                                        <li><a href="news-details.html#"><img src="https://demo.themeregion.com/newspress/images/others/s2.png" alt="" /></a></li>
                                                        <li><a href="news-details.html#"><img src="https://demo.themeregion.com/newspress/images/others/s3.png" alt="" /></a></li>
                                                        <li><a href="news-details.html#"><img src="https://demo.themeregion.com/newspress/images/others/s4.png" alt="" /></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div><!--/post-->
                                    </div><!--/.section-->
                                </div><!--/.left-content-->
                            </div>
                        </div>
                    </div><!--/#site-content-->

                    <div class="row">
                        <div class="col-sm-12">

                            <div class="comments-wrapper">
                                <h1 class="section-title title">{{ count($publish) }} Komentar</h1>
                                <ul class="media-list">
                                    {{-- @if($comments == null)) --}}
                                    @foreach ($comments as $comment)
                                    <li class="media">
                                        <div class="media-left">
                                            <a href="news-details.html#"><img class="media-object" src="https://demo.themeregion.com/newspress/images/others/author2.png" alt=""></a>
                                        </div>
                                        <div class="media-body">
                                            <h2>{{ $comment->nama }}</h2>
                                            <h3 class="date">{{ $comment->created_at }}</h3>
                                            @if ($comment->moderasi == 'no')
                                                <p><i>Komentar Anda sedang tahap moderasi</i></p>
                                            @else
                                            <p>{{ $comment->komentar }}</p>
                                            @endif
                                        </div>
                                    </li>
                                    @endforeach
                                    {{-- @endif --}}
                                    <div class="media media-child">
                                        <div class="media-left">
                                            <a href="news-details.html#"><img class="media-object" src="https://demo.themeregion.com/newspress/images/others/author3.png" alt=""></a>
                                        </div>
                                        <div class="media-body">
                                            <h2><a href="news-details.html#">Axel Bouaziz</a></h2>
                                             <h3 class="date"><a href="news-details.html#">15 December 2018</a></h3>
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>
                                            <a class="replay" href="news-details.html#">Replay</a>
                                        </div>
                                    </div>
                                </ul>

                                <div class="comments-box">
                                    <h1 class="section-title title">Tinggalkan Komentar</h1>
                                    <form id="comment-form" name="comment-form" method="post" action="{{ route('activitycomment.store') }}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="name">Nama</label>
                                                    <input type="text" name="name" class="form-control" required="required">
                                                    <input type="hidden" name="id_kegiatan" value="{{ $activity->id }}">
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="email" name="email" class="form-control" required="required">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="comment" >Komentar</label>
                                                    <textarea name="comment" id="comment" required="required" class="form-control" rows="5"></textarea>
                                                </div>
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-primary">Kirim </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.col-sm-9 -->

                @include('layouts.frontend_sidebar')

            </div>
        </div><!--/.section-->
    </div><!--/.container-->
@endsection

