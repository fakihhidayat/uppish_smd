
@extends('layouts.frontend')

@section('title')
    Pengumuman
@endsection

@section('spasi')
    <br>
@endsection

@section('content')
<div class="container">
    <div class="section">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <div id="site-content" class="site-content">
                        <div class="row">
                            <div class="col">
                                <div class="left-content">
                                    <div class="details-news">											
                                        <div class="post">
                                            <div class="entry-header">
                                                <div class="post-content">
                                                    <h2 class="entry-title">
                                                        {{ $ann->judul }}
                                                    </h2>
                                                    <div class="entry-meta">
                                                        <ul class="list-inline">
                                                            <li class="posted-by"><i class="fa fa-user"></i> by {{ $ann->admin_id }}</li>
                                                            <li class="publish-date"><i class="fa fa-clock-o"></i> {{ $ann->created_at }} </li>
                                                        </ul>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="post-content" style="margin-top: -50px">								
                                                <div class="entry-content">
                                                    <p>{!! nl2br(e($ann->deskripsi)) !!}</p>

                                                    <ul class="list-inline share-link">
                                                        <li><a href="news-details.html#"><img src="https://demo.themeregion.com/newspress/images/others/s1.png" alt="" /></a></li>
                                                        <li><a href="news-details.html#"><img src="https://demo.themeregion.com/newspress/images/others/s2.png" alt="" /></a></li>
                                                        <li><a href="news-details.html#"><img src="https://demo.themeregion.com/newspress/images/others/s3.png" alt="" /></a></li>
                                                        <li><a href="news-details.html#"><img src="https://demo.themeregion.com/newspress/images/others/s4.png" alt="" /></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div><!--/post--> 
                                    </div><!--/.section-->
                                </div><!--/.left-content-->
                            </div>
                        </div>
                    </div><!--/#site-content-->
                    
                </div><!--/.col-sm-9 -->	
                
                @include('layouts.frontend_sidebar')

            </div>				
        </div><!--/.section-->
    </div><!--/.container-->
@endsection

