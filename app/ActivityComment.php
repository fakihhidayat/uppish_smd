<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Activity;

class ActivityComment extends Model
{
    protected $fillable = 
    [
        'id_kegiatan', 'nama', 'email', 'komentar'
    ];

    public function getCreatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])->format('d, M Y');
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class, 'id_kegiatan', 'id');
    }
}