@extends('layouts.admin')

@section('title')
    SDM @if(isset($hr))- {{ $hr->nama_sdm }} @endif
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div>
            @endif
      </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="row">
                <div class="col-lg-10"><h4 class="card-title">SDM @if(isset($hr))| {{ $hr->nama_sdm }} @endif </h4></div>
                <div class="col-lg-2" align="right">
                  @if(isset($hr))
                    <a href="{{ route('employee.createNew', $hr->id)}}">
                        <button type="button" class="btn btn-primary btn-fw" style="margin-right: 4px;">
                        <i class="mdi mdi-plus"></i>Tambah</button>
                    </a>
                  @endif
                </div>
                </div>
                <div class="table-responsive table-sm">
                  @if(isset($group))
                  <table class="table table-bordered" id="datatable-news">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Wilayah Kerja</th>
                        <th>Foto</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Tempat Lahir</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        @if(count($group) > 0)
                        @foreach($group as $district => $employees)
                        <?php $jml = count($employees); ?>
                        <tr>
                            <td rowspan="{{ $jml }}">{{ $no++ }}</td>
                            <td rowspan="{{ $jml }}">{{ $district }}</td>
                            @foreach($employees as $e)
                            <td class="py-1">
                              <img src="{{ asset('asset_general/images/employee/'.$e->getEmployee->foto.'') }}" alt="image" />
                            </td>
                            <td>{{ $e->getEmployee->nik }}</td>
                            <td>{{ $e->getEmployee->nama }}</td>
                            <td>{{ $e->getEmployee->tmp_lahir }}</td>
                            <td><div class="btn-group" role="group" aria-label="Basic example">
                                  <button type="button" title="Ubah" class="btn btn-dark btn-xs"><a href="{{ route('employee.edit', $e->nik_pegawai) }}" style="color: white"><i class="mdi mdi-pencil"></i></a></button>
                                  <form action="{{ route('employee.destroy', $e->nik_pegawai) }}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" title="Hapus" class="btn btn-danger btn-xs"><i class="mdi mdi-delete"></i></button>
                                  </form>
                                </div>
                            </td>

                        </tr>
                        @endforeach
                        @endforeach
                        @else
                      <tr class="bg-secondary">
                        <td colspan="9" align="center">-- Tidak Ada Data --</td>
                      </tr>
                        @endif
                    </tbody>
                  </table>
                  @else
                  <table class="table table-bordered" id="datatable-news">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Foto</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Tempat Lahir</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php $no = 1; ?>
                       @if(count($employees) > 0)
                       @foreach ($employees as $e)
                       <tr>
                            <td>{{ $no++ }}</td>
                            <td class="py-1">
                              <img src="{{ asset('asset_general/images/employee/'.$e->getEmployee->foto.'') }}" alt="image" />
                            </td>
                            <td>{{ $e->nik_pegawai }}</td>
                            <td>{{ $e->getEmployee->nama }}</td>
                            <td>{{ $e->getEmployee->tmp_lahir }}</td>
                            <td><div class="btn-group" role="group" aria-label="Basic example">
                                  <button type="button" title="Ubah" class="btn btn-dark btn-xs"><a href="{{ route('employee.edit', $e->nik_pegawai) }}" style="color: white"><i class="mdi mdi-pencil"></i></a></button>
                                  <form action="{{ route('employee.destroy', $e->nik_pegawai) }}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" title="Hapus" class="btn btn-danger btn-xs"><i class="mdi mdi-delete"></i></button>
                                  </form>
                                </div>
                            </td>
                        </tr>
                       @endforeach
                       @else
                      <tr class="bg-secondary">
                        <td colspan="9" align="center">-- Tidak Ada Data --</td>
                      </tr>
                       @endif
                    </tbody>
                  </table>
                  @endif
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

@endsection
