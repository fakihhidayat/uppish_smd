
@extends('layouts.frontend')

@section('title')
    Info Terbaru
@endsection

@section('content')
<div class="container">
    <br>
        <div class="section">
            <div class="row">
                <div class="site-content col-lg-9">
                    <h1 class="section-title">Berita</h1>	
                    <div class="row">
                        @if(count($news) > 0)
                        @foreach($news as $new)
                        <div class="col-md-4">
                            <div class="post feature-post">
                                <div class="entry-header">
                                    <div class="entry-thumbnail">
                                        <img class="img-fluid" height="225" width="270" src="{{ asset('asset_general/images/news/'.$new->image.'') }}" alt="" />
                                    </div>
                                    <div class="catagory politics"><span><a href="">{{ $new->newsCategory->nama_kategori }}</a></span></div>
                                </div>
                                <div class="post-content">								
                                    <div class="entry-meta">
                                        <ul class="list-inline">
                                            <li class="publish-date"><i class="fa fa-clock"></i>{{ $new->created_at }}</li>
                                            <li class="views"><i class="fa fa-eye"></i>15k</li>
                                            <li class="loves"><i class="fa fa-comment"></i>{{ count($new->getComment) }}</li>
                                        </ul>
                                    </div>
                                    <h2 class="entry-title">
                                        <a href="{{ route('news.showById', $new->id) }}">{{ $new->judul }}</a>
                                    </h2>
                                </div>
                            </div><!--/post--> 	
                        </div>
                        @endforeach
                        @else
                        <div class="col-lg-12" style="margin-top: -3px">
                            <div class="left-content">
                                <div class="details-news">											
                                    <div class="post">
                                        <div class="post-content">								
                                            <div class="entry-content" align="center">
                                                <h3>-- Belum Ada Data --</h3>
                                            </div>
                                        </div>
                                    </div><!--/post--> 
                                </div><!--/.section-->
                            </div><!--/.left-content-->
                        </div>
                        @endif
                    </div>
                    <div class="pagination-wrapper">
                        @if ($news->hasPages())
                        <ul class="pagination">
                            @if ($news->onFirstPage())
                                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                                    <span aria-hidden="true">&lsaquo;</span>
                                </li>
                            @else
                                <li ><a href="{{ $news->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"><span aria-hidden="true"><i class="fa fa-long-arrow-left"></i> Previous Page</span></a></li>
                            @endif
    
                            @foreach ($news as $new)
                                @if (is_string($new))
                                    <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                                @endif
    
                                @if (is_array($new))
                                    @foreach ($new as $page => $url)
                                        @if ($page == $news->currentPage())
                                            <li class="active" aria-current="page"><span>{{ $page }}</span></li>
                                        @else
                                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                            
                            @if ($news->hasMorePages())
                                <li><a href="{{ $news->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"><span aria-hidden="true">Next Page <i class="fa fa-long-arrow-right"></i></span></a></li>
                            @else
                                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                                    <span aria-hidden="true">&rsaquo;</span>
                                </li>
                            @endif
                        </ul>
                        @endif
                    </div>
                </div><!--/#content--> 
            
            @include('layouts.frontend_sidebar')
        </div>				
    </div><!--/.section-->
    </div><!--/.container-->
@endsection

