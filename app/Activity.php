<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ActivityCategory;
use App\ActivityComment;
use Carbon;

class Activity extends Model
{
    protected $fillable =
    [
        'id_kategori','judul','tgl_kegiatan','lok_kegiatan','uraian','foto'
    ];

    public function getCreatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])->format('d, M Y');
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])->diffForHumans();
    }

    public function activityCategory()
    {
        return $this->belongsTo(ActivityCategory::class, 'id_kategori', 'id');
    }

    public function getComments()
    {
        return $this->hasMany(ActivityComment::class, 'id_kegiatan', 'id');
    }

}
