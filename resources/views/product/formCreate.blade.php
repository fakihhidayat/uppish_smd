@extends('layouts.admin')

@section('title')
    Post Produk Baru
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>
            
        <div class="col-md-12 grid-margin stretch-card">
                
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Pemberdayaan Ekonomi | Post Produk Baru</h4>
                <form class="forms-sample" action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                        <label for="nama">Nama Produk</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Produk" value="{{ old('nama') }}">
                        @if ($errors->has('nama'))
                        <small class="text-danger">{{ $errors->first('nama') }}</small>@endif
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Gambar/Foto</label>
                            <div class="input-group col-xs-12">
                                <input type="file" name="image" id="profile-img" class="form-control file-upload-info" placeholder="Gambar/Foto Produk" value="{{ old('image') }}">
                            </div>
                            @if ($errors->has('image'))
                            <small class="text-danger">{{ $errors->first('image') }}</small>@endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Preview</label><br>
                            <img src="{{ asset('asset_general/images/news/preview.png') }}" id="profile-img-tag" width="100px" height="100px" />
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="satuan">Satuan Jual</label>
                                <select name="satuan" id="satuan" placeholder="Satuan Produk" class="form-control">
                                    <option value="">-- Silahkan Pilih --</option>
                                    <option value="pcs">Pcs</option>
                                    <option value="kg">Kg</option>
                                </select>
                                @if ($errors->has('satuan'))
                                <small class="text-danger">{{ $errors->first('satuan') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="harga" class="harga">Harga</label>
                                <div class="input-group">
                                    <input type="text" name="harga" id="harga" class="form-control " placeholder="Harga Produk" value="{{ old('harga') }}" onkeypress="return hanyaAngka(event)">
                                </div>
                                @if ($errors->has('harga'))
                                <small class="text-danger">{{ $errors->first('harga') }}</small>@endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="form-control" id="deskripsi" rows="10" name="deskripsi">{{ old('deskripsi')}}</textarea>
                        @if ($errors->has('deskripsi'))
                        <small class="text-danger">{{ $errors->first('deskripsi') }}</small>@endif
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="{{ route('product.index') }}"><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">

jQuery(document).ready(function()
    {
        jQuery('select[name="satuan"]').on('change', function(){
            var satuan = jQuery(this).val();
            $('.harga').html('Harga /'+satuan);
        });
    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });

</script>

<script type="text/javascript">
		
    var rupiah = document.getElementById('harga');
    rupiah.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>

<script>
        function hanyaAngka(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))
    
            return false;
          return true;
        }
    </script>


@endpush