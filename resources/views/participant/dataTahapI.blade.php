@extends('layouts.admin')

@section('title')
    Total KPM
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div>
            @endif
      </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="row">
                <div class="col-lg-10"><h4 class="card-title">Total KPM | TAHAP I</h4></div>

                </div>
                <div class="table-responsive-sm">
                  <table class="table table-sm table-bordered" id="datatable-news">
                    <thead>
                      <tr class="bg-secondary">
                        <th>Kecamatan</th>
                        <th>Desa/Kelurahan</th>
                        <th>Jumlah KPM</th>
                        <th>Bumil</th>
                        <th>Balita</th>
                        <th>APRAS</th>
                        <th>SD</th>
                        <th>SMP</th>
                        <th>SMA</th>
                        <th>Lansia</th>
                        <th>Disabilitas</th>
                      </tr>
                    </thead>
                    <tbody>
                       @if(count($app) > 0)
                       @foreach($app as $key => $v)
                       <tr>
                        <td rowspan="{{ $v->getParticipantKategori->count()!=0 || $v->getParticipantKategori->count()!=null ? $v->getParticipantKategori->count()+1:1 }}">{{ $v->nama_kecamatan }}</td>

                           @foreach($v->getParticipantKategori as $detail => $d)
                           <tr>
                            <td id="number">{{ $d->getVillage->nama_desa }}</td>
                            <td id="number">{{ $d->jumlah }}</td>
                            <td id="number">{{ $d->bumil }}</td>
                            <td id="number">{{ $d->balita }}</td>
                            <td id="number">{{ $d->apras }}</td>
                            <td id="number">{{ $d->sd }}</td>
                            <td id="number">{{ $d->smp }}</td>
                            <td id="number">{{ $d->sma }}</td>
                            <td id="number">{{ $d->lansia }}</td>
                            <td id="number">{{ $d->disabilitas }}</td>
                           </tr>
                           @endforeach
                           <tr>
                            <td colspan="2"><b>{{$v->nama_kecamatan}} TOTAL</b></td>
                            <td><b>{{($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('jumlah')->toArray())) : 0)}}</b></td>
                            <td>{{($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('bumil')->toArray())) : 0)}}</td>
                            <td>{{($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('balita')->toArray())) : 0)}}</td>
                            <td>{{($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('apras')->toArray())) : 0)}}</td>
                            <td>{{($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('sd')->toArray())) : 0)}}</td>
                            <td>{{($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('smp')->toArray())) : 0)}}</td>
                            <td>{{($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('sma')->toArray())) : 0)}}</td>
                            <td>{{($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('lansia')->toArray())) : 0)}}</td>
                            <td>{{($v->getParticipantKategori->count()!=0 ? array_sum(array_map('intval',$v->getParticipantKategori->pluck('disabilitas')->toArray())) : 0)}}</td>
                           </tr>

                       @endforeach

                        </tr>
                       @else
                        <tr class="bg-secondary">
                          <td colspan="12" align="center">-- Tidak Ada Data --</td>
                        </tr>
                       @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

@endsection



