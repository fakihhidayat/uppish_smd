<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
use App\Profile;
use App\SubDistrict;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sepa = Profile::where('jenis_profile', 'Selayang Pandang')->first();
        $profiles = Profile::where('jenis_profile', 'Profil')->first();
        $visions = Profile::where('jenis_profile', 'Visi')->first();
        $missions = Profile::where('jenis_profile', 'Misi')->first();
        $tf = Profile::where('jenis_profile', 'Tugas dan Fungsi')->first();
        return view('profile.index', compact(['profiles','visions','missions','tf','sepa']));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required'
        ]);

        $profile = new Profile;
        $profile->jenis_profile = $request->get('jenis_profile');
        $profile->isi = $request->get('isi');


        $file = Request()->file('image');

        if($file) {
            $fileName   = $file->getClientOriginalName();
            Request()->file('image')->move(public_path('asset_general/images/profile'), $fileName);
            $profile->image = $fileName;
        }

        try {
            $profile->save();
            return redirect()->route('profile.index')->with('success','Profile berhasil diisi !');
        }
        catch (\Exception $e) {
            return back()->with('error','Profile gagal diisi !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id);
        return view('profile.formEdit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'isi' => 'required'
        ]);

        $profile = Profile::find($id);
        $profile->jenis_profile = $request->get('jenis_profile');
        $profile->isi = $request->get('isi');


        $file = Request()->file('image');

        if($file) {
            $fileName   = $file->getClientOriginalName();
            Request()->file('image')->move(public_path('asset_general/images/profile'), $fileName);
            $profile->image = $fileName;
        }

        try {
            $profile->save();
            return redirect()->route('profile.index')->with('success','Profile berhasil diupdate !');
        }
        catch (\Exception $e) {
            return back()->with('error','Profile gagal diupdate !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showPkh()
    {
        $profiles = Profile::where('jenis_profile', 'Profil')->first();
        return view('profile.pkh', compact('profiles'));
    }
    public function showVisiMisi()
    {
        $visions = Profile::where('jenis_profile', 'Visi')->first();
        $missions = Profile::where('jenis_profile', 'Misi')->first();
        return view('profile.visimisi', compact(['visions', 'missions']));
    }
    public function showTugasFungsi()
    {
        $tf = Profile::where('jenis_profile', 'Tugas dan Fungsi')->first();
        return view('profile.tugasfungsi', compact('tf'));
    }

    public function createNew($jenis)
    {
        $request = $jenis;
        return view('profile.formCreate', compact('request'));
    }
}
