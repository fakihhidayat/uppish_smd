<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Admin $admin)
    {
        return $this->view('authAdmmin.mail')->with([
            'link'=>route('activating-account',$this->admin->token_register)
        ]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         
    }
}
