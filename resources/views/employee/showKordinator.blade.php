
@extends('layouts.frontend')

@section('title')
    SDM @isset($kokab)- {{ $kokab->getHumanResource->nama_sdm }} @endisset
@endsection

@section('content')
<div class="container">
    <br>
        <div class="section">
            <div class="row">
                @if(isset($kokab))
                <div class="col-md-8 col-lg-9">
                    <div id="site-content" class="section world-news">
                            <h1 class="section-title">{{ $kokab->getHumanResource->nama_sdm }}</h1>	
                            <div class="world-nav cat-menu">         
                                <ul class="list-inline">                      
                                    <li><a href="{{ route('humanresource.detail', $kokab->getHumanResource->id) }}" class="btn-show" title="{{ $kokab->getHumanResource->nama_sdm }}">Deskripsi Pekerjaan</a></li>
                                </ul> 					
                            </div>
                        <div class="author-details">
                            <div class="author-heading">
                                <div class="author-profile">
                                    <div class="author-gravatar">
                                        <img class="img-fluid" width="150" height="150" src="{{ asset('asset_general/images/employee/'.$kokab->getEmployee->foto.'') }}" alt="" />
                                    </div>
                                    <div class="author-name">
                                        <h1>{{ $kokab->getEmployee->nama }}</h1>
                                        <p>{{ $kokab->getHumanResource->nama_sdm }}</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="author-info">

                                        <h3>Tempat Lahir</h3>
                                        <p>{{ $kokab->getEmployee->tmp_lahir }}</p><br>
        
                                        <h3>Tanggal Lahir</h3>
                                        <p>{{ date('d, F Y', strtotime($kokab->getEmployee->tgl_lahir)) }}</p><br>

                                        <h3>Alamat</h3>
                                        <p>{{ $kokab->getEmployee->alamat }}</p><br>
        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="author-info">
                                        <h3>E-mail</h3>
                                        <p>{{ $kokab->getEmployee->email }}</p><br>
        
                                        <h3>No. Telp</h3>
                                        <p>{{ $kokab->getEmployee->telp }}</p><br>
        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div><!--/#site-content-->
                </div>
                @else
                <div class="col-lg-9" style="margin-top: -3px">
                    <div class="left-content">
                        <div class="details-news">											
                            <div class="post">
                                <div class="post-content">								
                                    <div class="entry-content" align="center">
                                        <h3>-- Belum Ada Data --</h3>
                                    </div>
                                </div>
                            </div><!--/post--> 
                        </div><!--/.section-->
                    </div><!--/.left-content-->
                </div>
                @endif
                @include('layouts.frontend_sidebar')
            </div>				
        </div><!--/.section-->
    </div><!--/.container-->
@endsection
