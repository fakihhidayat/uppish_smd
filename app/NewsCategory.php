<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\News;
use app\Video;

class NewsCategory extends Model
{
    protected $fillable = [
        'nama_kategori', 'deskripsi'
    ];

    public function news()
    {
        return $this->hasMany(News::class, 'id_kategori', 'id');
    }

    public function video()
    {
        return $this->hasMany(Video::class, 'id_kategori', 'id');
    }
}
