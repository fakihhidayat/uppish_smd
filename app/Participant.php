<?php

namespace App;
use App\ParticipantsCategory;
use App\Village;
use App\SubDistrict;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $fillable =
    [
        'nomor_pkh','nama','alamat','id_detail','id_village'
    ];

    public function getDetail()
    {
        return $this->belongsTo(ParticipantsCategory::class, 'id', 'id_detail');
    }

    


}
