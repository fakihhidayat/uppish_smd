
@extends('layouts.frontend')

@section('title')
    Kegiatan
@endsection

@section('content')
<div class="container">
    <br>
        <div class="section">
        <div class="row">
            <div class="col-md-8 col-lg-9">
                <div class="section world-news">
                <h1 class="section-title">{{ $category->kat_kegiatan }} </h1>
                    <div class="world-nav cat-menu">
                        <ul class="list-inline">
                            <li><a href="#" data-toggle="modal" data-target="#modal">Apa Itu {{ $category->kat_kegiatan }} ?</a></li>
                        </ul>
                    </div>
                    <div class="section">
                        <div class="row">
                            @if(count($activities) > 0)
                            @foreach ($activities as $activity)
                            <div class="col-md-6 col-lg-4">
                                <div class="post medium-post">
                                    <div class="entry-header">
                                        <div class="entry-thumbnail">
                                            <img class="img-fluid" src="{{ asset('asset_general/images/activity/'.$activity->foto.'') }}" alt="" />
                                        </div>
                                    </div>
                                    <div class="post-content">
                                        <div class="entry-meta">
                                            <ul class="list-inline">
                                                <li class="publish-date"><i class="fa fa-clock"></i>{{ $activity->tgl_kegiatan }}</li>
                                                <li class="views"><i class="fas fa-map-marker-alt"></i>{{ $activity->lok_kegiatan }}</li>
                                            </ul>
                                        </div>
                                        <h2 class="entry-title">
                                            <a href="{{ route('activity.showById', $activity->id) }}">{{ $activity->judul }}</a>
                                        </h2>
                                    </div>
                                </div><!--/post-->
                            </div>
                            @endforeach
                            @else
                            <div class="col-lg-12" style="margin-top: -3px">
                                <div class="left-content">
                                    <div class="details-news">
                                        <div class="post">
                                            <div class="post-content">
                                                <div class="entry-content" align="center">
                                                    <h3>-- Belum Ada Data --</h3>
                                                </div>
                                            </div>
                                        </div><!--/post-->
                                    </div><!--/.section-->
                                </div><!--/.left-content-->
                            </div>
                            @endif
                        </div>
                        <div class="row">
                                <div class="col-lg-12" style="margin-top: -3px">
                                        <div class="left-content">
                                            <div class="details-news">
                                                <div class="post">
                                                    <div class="post-content">
                                                            <canvas id="myChart" width="400" height="200"></canvas>
                                                    </div>
                                                </div><!--/post-->
                                            </div><!--/.section-->
                                        </div><!--/.left-content-->
                                    </div>

                        </div>
                    </div><!--/.section -->
                </div><!--/.section-->

                <div class="pagination-wrapper">
                    @if ($activities->hasPages())
                    <ul class="pagination">
                        @if ($activities->onFirstPage())
                            <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                                <span aria-hidden="true">&lsaquo;</span>
                            </li>
                        @else
                            <li ><a href="{{ $activities->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"><span aria-hidden="true"><i class="fa fa-long-arrow-left"></i> Previous Page</span></a></li>
                        @endif

                        @foreach ($activities as $activity)
                            @if (is_string($activity))
                                <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                            @endif

                            @if (is_array($activity))
                                @foreach ($activity as $page => $url)
                                    @if ($page == $activities->currentPage())
                                        <li class="active" aria-current="page"><span>{{ $page }}</span></li>
                                    @else
                                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach

                        @if ($activities->hasMorePages())
                            <li><a href="{{ $activities->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"><span aria-hidden="true">Next Page <i class="fa fa-long-arrow-right"></i></span></a></li>
                        @else
                            <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                                <span aria-hidden="true">&rsaquo;</span>
                            </li>
                        @endif
                    </ul>
                    @endif

                </div>
            </div>
            @include('layouts.frontend_sidebar')
        </div>
    </div><!--/.section-->
    </div><!--/.container-->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">{{ $category->kat_kegiatan }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body break-word">
                    {!! $category->deskripsi !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
                </div>
            </div>
        </div>

        <style>
        .break-word {
             word-wrap: break-word;
        }
        </style>

@endsection
@push('scripts')
<script>
        $.get('http://localhost/uppkh_smd/public/api/chartJs',function(response){
            var tahap = [] ;
            var data1 = [] ;
            var data2 = [] ;
            var data3 = [] ;
            var data4 = [] ;
            response.datashets.forEach((element,index) => {
                element.forEach(hasil => {
                    if(hasil.tahap=="1")
                    {
                        if(!tahap.includes(1))
                        {
                            tahap.push(1)
                        }
                        tahap.includes(1)?data1.push(hasil.data):data1.push(0)
                    }else if(hasil.tahap=="2")
                    {
                        if(!tahap.includes(2))
                        {
                            tahap.push(2)
                        }
                        data2.push(hasil.data)
                    }else if(hasil.tahap=="3")
                    {
                        if(!tahap.includes(3))
                        {
                            tahap.push(3)
                        }
                        data3.push(hasil.data)
                    }else if(hasil.tahap=="4")
                    {
                        if(!tahap.includes(4))
                        {
                            tahap.push(4)
                        }
                        data4.push(hasil.data)
                    }
                });
            });
            console.log(tahap   )
            var ctx = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: response.labels,
                datasets: [
                    {
                         label: 'Tahap 1',
                        data: data1,
                    },
                    {
                        label: 'Tahap 2',
                        data: data2
                    },
                    {
                    label: 'Tahap 3',
                    data: data3,
                    },
                    {
                    label: 'Tahap 4',
                    data: data4
                    },
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
        })


        </script>
@endpush
