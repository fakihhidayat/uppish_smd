<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Charts;
use App\SubDistrict;
use App\Village;
use App\ParticipantsCategory;


class SubDistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $district = SubDistrict::orderBy('nama_kecamatan')->get();
        $data = Village::with('subdisctrict')->get();
            $group = $data->groupBy(function ($dis){
            return $dis->subdisctrict->nama_kecamatan;
        });
        return view('subdistrict.index', compact(['group','district']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subdistrict.formCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'profil' => 'required',
            'lat' => 'required',
            'long' => 'required'
        ]);

        $district = new SubDistrict([
            'nama_kecamatan' => $request->get('kecamatan'),
            'profil' => $request->get('profil'),
            'lat' => $request->get('lat'),
            'long' => $request->get('long')
        ]);

        try {
            $district->save();
            return redirect()->route('subdistrict.index')->with('success','Kecamatan berhasil ditamabh !');
        }
        catch (\Exception $e) {
            return back()->with('error','Kecamatan gagal ditambah !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->admin != 1)
        {
            dd("Tidak Memilik akses!");
        }

        $subdistrict = SubDistrict::find($id);

        $vill = VIllage::where('id_subdistrict', $id)->get();


       /* $chart  = Charts::database($vill, 'pie', 'highcharts')
                        ->title("Penerima Bantuan PKH")
                        ->elementLabel('Jumlah KK')
                        ->labels($vill->pluck('nama_desa'))
                        ->values($vill->pluck('jml_penerima_bantuan'))
                        ->dimensions(750, 500)
                        ->responsive(false);*/
        $data['chart'] = ParticipantsCategory::where('id_district',$id)->first();

        // foreach ($villages as $village) {
        //     $data[] =
        //     [
        //         'desa' => $village->nama_desa,
        //         'jml_bantuan' => $village->jml_penerima_bantuan,
        //         'ket' => $village->keterangan
        //     ];
        // }
        return view ('subdistrict.show', compact('subdistrict'), $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $district = SubDistrict::where('id', $id)->first();
        return view('subdistrict.formEdit', compact('district'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'lat' => 'required',
            'long' => 'required',
            'profil' => 'required'
        ]);

        $district = SubDistrict::find($id);
        $district->nama_kecamatan = $request->get('kecamatan');
        $district->lat = $request->get('lat');
        $district->long = $request->get('long');
        $district->profil = $request->get('profil');

        try {
            $district->save();
            return redirect()->route('subdistrict.index')->with('success','Kecamatan berhasil diupdate !');
        }
        catch (\Exception $e) {
            return back()->with('error','Kecamatan gagal diupdate !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SubDistrict::destroy($id);
        Village::where('id_kecamatan', $id)->delete();
        return back()->with('success', 'Kecamatan berhasil dihapus!');
    }
}
