@extends('layouts.frontend')

@section('title')
    Home
@endsection

@section('content')
@include('layouts.frontend_slider')

<div class="container">		
        <div id="breaking-news">
				<span><a href="{{ route('announcement.showAll') }}">Pengumuman</a></span>
				<div class="breaking-news-scroll">
					<ul>
                        @if(count($announcements) > 0)
                        @foreach ($announcements as $announcement)
                        <li><i class="fa fa-angle-double-right"></i>
                            <a href="{{ route('announcement.showById', $announcement->id) }}" title="">{{ $announcement['judul'] }}</a>
						</li>
                        @endforeach
                        @else
                        <li><i class="fa fa-angle-double-right"></i>
                            <a href="" title="">Tidak Ada Pengumuman</a>
						</li>
                        @endif
					</ul>
				</div>
			</div><!--#breaking-news-->
    
    {{-- <div class="section">				
        <div class="latest-news-wrapper">
            <h1 class="section-title"><span>Berita Terbaru</span></h1>	
            <div id="latest-news">
                <div class="post medium-post">
                    <div class="entry-header">
                        <div class="entry-thumbnail">
                            <img class="img-fluid" src="images/post/1.jpg" alt="" />
                        </div>
                        <div class="catagory politics"><span><a href="index5.html#">Politics</a></span></div>
                    </div>
                    <div class="post-content">								
                        <div class="entry-meta">
                            <ul class="list-inline">
                                <li class="publish-date"><a href="index5.html#"><i class="fa fa-clock-o"></i> Nov 5, 2018 </a></li>
                                <li class="views"><a href="index5.html#"><i class="fa fa-eye"></i>15k</a></li>
                                <li class="loves"><a href="index5.html#"><i class="fa fa-heart-o"></i>278</a></li>
                            </ul>
                        </div>
                        <h2 class="entry-title">
                            <a href="news-details.html">Our closest relatives aren't fans of daylight saving time</a>
                        </h2>
                    </div>
                </div><!--/post--> 
                <div class="post medium-post">
                    <div class="entry-header">
                        <div class="entry-thumbnail">
                            <img class="img-fluid" src="images/post/2.jpg" alt="" />
                        </div>
                        <div class="catagory world"><a href="index5.html#">World</a></div>
                    </div>
                    <div class="post-content">								
                        <div class="entry-meta">
                            <ul class="list-inline">
                                <li class="publish-date"><a href="index5.html#"><i class="fa fa-clock-o"></i> Nov 5, 2018 </a></li>
                                <li class="views"><a href="index5.html#"><i class="fa fa-eye"></i>15k</a></li>
                                <li class="loves"><a href="index5.html#"><i class="fa fa-heart-o"></i>278</a></li>
                            </ul>
                        </div>
                        <h2 class="entry-title">
                            <a href="news-details.html">44 books on 44 presidents: Welcome, folks, to the Adams</a>
                        </h2>
                    </div>
                </div><!--/post--> 
                <div class="post medium-post">
                    <div class="entry-header">
                        <div class="entry-thumbnail">
                            <img class="img-fluid" src="images/post/4.jpg" alt="" />
                        </div>
                        <div class="catagory health"><span><a href="index5.html#">Health</a></span></div>
                    </div>
                    <div class="post-content">								
                        <div class="entry-meta">
                            <ul class="list-inline">
                                <li class="publish-date"><a href="index5.html#"><i class="fa fa-clock-o"></i> Nov 5, 2018 </a></li>
                                <li class="views"><a href="index5.html#"><i class="fa fa-eye"></i>15k</a></li>
                                <li class="loves"><a href="index5.html#"><i class="fa fa-heart-o"></i>278</a></li>
                            </ul>
                        </div>
                        <h2 class="entry-title">
                            <a href="news-details.html">And the most streamed Beatles song on Spotify is...</a>
                        </h2>
                    </div>
                </div><!--/post--> 

                <div class="post medium-post">
                    <div class="entry-header">
                        <div class="entry-thumbnail">
                            <img class="img-fluid" src="images/post/3.jpg" alt="" />
                        </div>
                        <div class="catagory sports"><span><a href="index5.html#">Sports</a></span></div>
                    </div>
                    <div class="post-content">								
                        <div class="entry-meta">
                            <ul class="list-inline">
                                <li class="publish-date"><a href="index5.html#"><i class="fa fa-clock-o"></i> Nov 5, 2018 </a></li>
                                <li class="views"><a href="index5.html#"><i class="fa fa-eye"></i>15k</a></li>
                                <li class="loves"><a href="index5.html#"><i class="fa fa-heart-o"></i>278</a></li>
                            </ul>
                        </div>
                        <h2 class="entry-title">
                            <a href="news-details.html">NBA suspends Matt Barnes for attack on Derek Fisher</a>
                        </h2>
                    </div>
                </div><!--/post--> 
                
                <div class="post medium-post">
                    <div class="entry-header">
                        <div class="entry-thumbnail">
                            <img class="img-fluid" src="images/post/5.jpg" alt="" />
                        </div>
                        <div class="catagory politics"><span><a href="index5.html#">Politics</a></span></div>
                    </div>
                    <div class="post-content">								
                        <div class="entry-meta">
                            <ul class="list-inline">
                                <li class="publish-date"><a href="index5.html#"><i class="fa fa-clock-o"></i> Nov 5, 2018 </a></li>
                                <li class="views"><a href="index5.html#"><i class="fa fa-eye"></i>15k</a></li>
                                <li class="loves"><a href="index5.html#"><i class="fa fa-heart-o"></i>278</a></li>
                            </ul>
                        </div>
                        <h2 class="entry-title">
                            <a href="news-details.html">At Facebook who suggests tags for all of your photos</a>
                        </h2>
                    </div>
                </div><!--/post--> 

                <div class="post medium-post">
                    <div class="entry-header">
                        <div class="entry-thumbnail">
                            <img class="img-fluid" src="images/post/6.jpg" alt="" />
                        </div>
                        <div class="catagory entertainment"><span><a href="index5.html#">Entertainment</a></span></div>
                    </div>
                    <div class="post-content">								
                        <div class="entry-meta">
                            <ul class="list-inline">
                                <li class="publish-date"><a href="index5.html#"><i class="fa fa-clock-o"></i> Nov 5, 2018 </a></li>
                                <li class="views"><a href="index5.html#"><i class="fa fa-eye"></i>15k</a></li>
                                <li class="loves"><a href="index5.html#"><i class="fa fa-heart-o"></i>278</a></li>
                            </ul>
                        </div>
                        <h2 class="entry-title">
                            <a href="news-details.html">Suspicion of plotting New Year's Eve attacks in Belgium</a>
                        </h2>
                    </div>
                </div><!--/post--> 
                <div class="post medium-post">
                    <div class="entry-header">
                        <div class="entry-thumbnail">
                            <img class="img-fluid" src="images/post/9.jpg" alt="" />
                        </div>
                        <div class="catagory sports"><span><a href="index5.html#">Sports</a></span></div>
                    </div>
                    <div class="post-content">								
                        <div class="entry-meta">
                            <ul class="list-inline">
                                <li class="publish-date"><a href="index5.html#"><i class="fa fa-clock-o"></i> Nov 5, 2018 </a></li>
                                <li class="views"><a href="index5.html#"><i class="fa fa-eye"></i>15k</a></li>
                                <li class="loves"><a href="index5.html#"><i class="fa fa-heart-o"></i>278</a></li>
                            </ul>
                        </div>
                        <h2 class="entry-title">
                            <a href="news-details.html">Someone has calculated the total cost of saving Matt</a>
                        </h2>
                    </div>
                </div><!--/post--> 
                
                <div class="post medium-post">
                    <div class="entry-header">
                        <div class="entry-thumbnail">
                            <img class="img-fluid" src="images/post/8.jpg" alt="" />
                        </div>
                        <div class="catagory politics"><span><a href="index5.html#">Politics</a></span></div>
                    </div>
                    <div class="post-content">								
                        <div class="entry-meta">
                            <ul class="list-inline">
                                <li class="publish-date"><a href="index5.html#"><i class="fa fa-clock-o"></i> Nov 5, 2018 </a></li>
                                <li class="views"><a href="index5.html#"><i class="fa fa-eye"></i>15k</a></li>
                                <li class="loves"><a href="index5.html#"><i class="fa fa-heart-o"></i>278</a></li>
                            </ul>
                        </div>
                        <h2 class="entry-title">
                            <a href="news-details.html">Homemade lightsaber made with laser can cut wires</a>
                        </h2>
                    </div>
                </div><!--/post--> 

                <div class="post medium-post">
                    <div class="entry-header">
                        <div class="entry-thumbnail">
                            <img class="img-fluid" src="images/post/10.jpg" alt="" />
                        </div>
                        <div class="catagory entertainment"><span><a href="index5.html#">Entertainment</a></span></div>
                    </div>
                    <div class="post-content">								
                        <div class="entry-meta">
                            <ul class="list-inline">
                                <li class="publish-date"><a href="index5.html#"><i class="fa fa-clock-o"></i> Nov 5, 2018 </a></li>
                                <li class="views"><a href="index5.html#"><i class="fa fa-eye"></i>15k</a></li>
                                <li class="loves"><a href="index5.html#"><i class="fa fa-heart-o"></i>278</a></li>
                            </ul>
                        </div>
                        <h2 class="entry-title">
                            <a href="news-details.html">Our closest relatives aren't fans of daylight saving time</a>
                        </h2>
                    </div>
                </div><!--/post--> 
            </div>
        </div><!--/.latest-news-wrapper-->
    </div><!--/.section--> --}}
    
    <div class="section">
        <div class="row">
            <div class="col-lg-9 col-md-8">
                <div id="site-content" class="site-content">
                    <div class="row">
                        <div class="col-md-6 col-lg-8">
                            <div class="left-content">
                                <div class="section world-news">
                                    <h1 class="section-title title">Selayang Pandang</h1>	
                                    <div class="post">
                                        @if(isset($profiles))
                                        <div class="entry-header">
                                            <div class="entry-thumbnail">
                                            @if(isset($profiles->image))
                                            <img class="img-fluid" height="325" width="570" src="{{ asset('asset_general/images/profile/'.$profiles->image.'') }}" alt="" />
                                            @endif
                                            </div>
                                        </div>
                                        <div class="post-content">
                                            <div class="entry-content">
                                                <p>{{ $profiles->isi }}</p>
                                            </div>
                                        </div>
                                        @else
                                        <div class="col-lg-12" style="margin-top: -3px">
                                            <div class="left-content">
                                                <div class="details-news">											
                                                    <div class="post">
                                                        <div class="post-content">								
                                                            <div class="entry-content" align="center">
                                                                <h3>-- Belum Ada Data --</h3>
                                                            </div>
                                                        </div>
                                                    </div><!--/post--> 
                                                </div><!--/.section-->
                                            </div><!--/.left-content-->
                                        </div>
                                        @endif
                                        <div class="list-post">
                                            <ul>
                                                <li><a href="{{ route('profile.pkh') }}">Profil Selengkapnya <i class="fa fa-angle-right"></i></a></li>
                                                <li><a href="{{ route('profile.visimisi') }}">Visi & Misi<i class="fa fa-angle-right"></i></a></li>
                                                <li><a href="{{ route('profile.tugasfungsi') }}">Tugas & Fungsi <i class="fa fa-angle-right"></i></a></li>
                                            </ul>
                                        </div><!--/list-post--> 
                                    </div><!--/post--> 
                                </div><!--/.section-->
                                
                            </div><!--/.left-content-->
                        </div>
                        <div class="col-lg-4 col-md-6 tr-sticky">
                            <div class="middle-content theiaStickySidebar">
                      
                                
                                
                                <div class="section video-section">
                                    <h1 class="section-title title">Video Terbaru</h1>
                                    @if(count($videos) > 0)
                                    @foreach($videos as $video)
                                    <div class="post video-post medium-post">
                                        <div class="entry-header">
                                            <div class="entry-thumbnail embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $video->url_youtube }}" frameborder="0" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="post-content">								
                                            <div class="video-catagory"><a href="">{{ $video->getVideoCategory->nama_kategori }}</a></div>
                                            <h2 class="entry-title">
                                                <a href="https://www.youtube.com/watch?v={{ $video->url_youtube }}" target="_blank">{{ $video->judul }}</a>
                                            </h2>
                                        </div>
                                    </div><!--/post-->
                                    @endforeach	
                                    @else
                                    <div class="post video-post medium-post">
                                        <div class="post-content">
                                            <h2 class="entry-title">
                                               <p>-- Belum Ada Data yang Tersedia --</p>
                                            </h2>
                                        </div>
                                    </div><!--/post-->
                                    @endif										
                                </div> <!-- /.video-section -->
                          
                            </div><!--/.middle-content-->
                        </div>
                    </div>
                </div><!--/#site-content-->
            </div>
            @include('layouts.frontend_sidebar')
        </div>				
    </div><!--/.section-->
</div><!--/.container-->
@endsection