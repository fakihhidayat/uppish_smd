@extends('layouts.admin')

@section('title')
    Kecamatan & Desa 
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="flash-message">
                <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            </div>
            @endif
      </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Kecamatan & Desa</h4>
                <div class="row">
                  <div class="col-lg-4">
                      <div class="table-responsive">
                          <h5 class="card-title">Kecamatan</h5>
                          <table class="table table-bordered table-sm">
                            <thead>
                              <tr class="bg-secondary">
                                <th>Aksi</th>
                                <th>Kecamatan | <a href="{{ route('subdistrict.create') }}">+ Tambah</a></th>
                                <th>Input</th>
                              </tr>
                            </thead>
                            <tbody>
                              @if(count($district) > 0)
                              @foreach ($district as $dis)
                              <tr>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                          <button type="button" title="Ubah" class="btn btn-dark btn-xs"><a href="{{ route('subdistrict.edit', $dis->id) }}" style="color: white"><i class="mdi mdi-pencil"></i></a></button>
                                          <form action="{{ route('subdistrict.destroy', $dis->id) }}" method="POST">
                                          @csrf
                                          @method('DELETE')
                                          <button type="submit" title="Hapus" class="btn btn-danger btn-xs"><i class="mdi mdi-delete"></i></button>
                                          </form>
                                        </div>
                                    </td>
                                    <td>{{ $dis->nama_kecamatan }}</td>
                                    <td><a href="{{ route('village.createNew', $dis->id) }}">+ Desa</a></td>
                                </tr>
                              @endforeach 
                              @else
                                <tr class="bg-secondary">
                                  <td colspan="6" align="center">-- Tidak Ada Data --</td>
                                </tr>
                              @endif
                            </tbody>
                          </table>
                        </div>
                  </div>
                  <div class="col-lg-8">
                    <div class="table-responsive">
                        <h5 class="card-title">Desa</h5>
                      <table class="table table-bordered table-sm" id="datatable-news">
                        <thead>
                          <tr class="bg-secondary">
                            <th>Kecamatan</th>
                            <th>Desa</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if(count($group) > 0)
                          @foreach ($group as $district => $village)
                          <?php $jml = count($village); ?>
                          <tr>
                              <td rowspan="{{ $jml }}">{{ $district }}</td>
                              @foreach ($village as $v)
                              <td>{{ $v->nama_desa }}</td>
                              <td><div class="btn-group" role="group" aria-label="Basic example">
                                    <button type="button" title="Ubah" class="btn btn-dark btn-xs"><a href="{{ route('village.edit', $v->id) }}" style="color: white"><i class="mdi mdi-pencil"></i></a></button>
                                    <form action="{{ route('village.destroy', $v->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" title="Hapus" class="btn btn-danger btn-xs"><i class="mdi mdi-delete"></i></button>
                                    </form>
                                  </div>
                              </td>
                            </tr>
                          @endforeach
                          @endforeach 
                          @else
                            <tr class="bg-secondary">
                              <td colspan="6" align="center">-- Tidak Ada Data --</td>
                            </tr>
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

@endsection