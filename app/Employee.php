<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\HumanResource;
use App\Job;


class Employee extends Model
{
    protected $guarded = [];

    public function getHumanResource()
    {
        return $this->belongsTo(HumanResource::class, 'id_humanresource', 'id');
    }

    public function getJob()
    {
        return $this->hasOne(Job::class, 'nik_pegawai', 'nik');
    }
}
