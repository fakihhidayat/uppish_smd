
@extends('layouts.frontend')

@section('title')
SDM - {{ $hr->nama_sdm }}
@endsection

@section('content')
<div class="container">
    <br>
        <div class="section">
            <div class="row">
                @if(count($otherEmployee) > 0)
                <div class="col-md-8 col-lg-9">
                    <div class="section world-news">
                        <h1 class="section-title">{{ $hr->nama_sdm }}</h1>
                        <div class="world-nav cat-menu">
                            <ul class="list-inline">
                                <li><a href="{{ route('humanresource.detail', $hr->id) }}" class="btn-show" title="{{ $hr->nama_sdm }}">Deskripsi Pekerjaan</a></li>
                            </ul>
                        </div>
                        <div class="post">
                            <div class="post-content">
                                <div class="entry-content">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="table-primary">
                                            <th scope="col">No</th>
                                            <th scope="col">Nama Lengkap</th>
                                            <th scope="col">Tempat Lahir</th>
                                            <th scope="col">Detail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            ?>
                                            @foreach ($otherEmployee as $e)
                                            <tr>
                                                <td>{{ $no++ }}</th>
                                                <td>{{ $e->getEmployee->nama }}</td>
                                                <td>{{ $e->getEmployee->tmp_lahir }}</td>
                                                <td><a href="{{ route('employee.detail', $e->getEmployee->nik) }}" class="btn-show" title="Detail {{ $e->getEmployee->nama }}"><i class="fa fa-eye"></i></a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/post-->
                    </div><!--/.section-->
                </div>
                @else
                <div class="col-lg-9" style="margin-top: -3px">
                    <div class="left-content">
                        <div class="details-news">
                            <div class="post">
                                <div class="post-content">
                                    <div class="entry-content" align="center">
                                        <h3>-- Belum Ada Data --</h3>
                                    </div>
                                </div>
                            </div><!--/post-->
                        </div><!--/.section-->
                    </div><!--/.left-content-->
                </div>
                @endif
                @include('layouts.frontend_sidebar')
            </div>
        </div><!--/.section-->
    </div><!--/.container-->
@endsection
