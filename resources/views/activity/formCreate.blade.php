@extends('layouts.admin')

@section('title')
    Post Kegiatan Baru
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
        </div>

        <div class="col-md-12 grid-margin stretch-card">

            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Aktivitas | Post Kegiatan Baru</h4>
                <form class="forms-sample" action="{{ route('activity.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Kegiatan" value="{{ old('judul') }}">
                        @if ($errors->has('judul'))
                        <small class="text-danger">{{ $errors->first('judul') }}</small>@endif
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kategori">Kategori</label>
                                {!! Form::select('kategori', $kategori, null, ['class'=>'form-control', 'placeholder'=> '-- Kategori Kegiatan --', 'value' => '{{ old("kategori")}}']) !!}
                                @if ($errors->has('kategori'))
                                <small class="text-danger">{{ $errors->first('kategori') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Gambar/Foto</label>
                                <div class="input-group col-xs-12">
                                    <input type="file" name="image" id="profile-img" class="form-control file-upload-info" placeholder="Gambar/Foto Kegiatan" value="{{ old('image') }}">
                                </div>
                                @if ($errors->has('image'))
                                <small class="text-danger">{{ $errors->first('image') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-2">
                                <div class="form-group">
                                    <label>Preview</label><br>
                                    <img src="{{ asset('asset_general/images/news/preview.png') }}" id="profile-img-tag" width="100px" height="100px" />
                                </div>
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tgl">Tanggal</label>
                                <div class="input-group col-xs-12">
                                    <input type="date" name="tgl" id="tgl_kegiatan" class="form-control" placeholder="Tanggal Kegiatan" value="{{ old('tgl') }}">
                                </div>
                                @if ($errors->has('tgl'))
                                <small class="text-danger">{{ $errors->first('tgl') }}</small>@endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lokasi">Lokasi</label>
                                <div class="input-group col-xs-12">
                                    <input type="text" name="lokasi" class="form-control" placeholder="Lokasi Kegiatan" value="{{ old('lokasi') }}">
                                </div>
                                @if ($errors->has('lokasi'))
                                <small class="text-danger">{{ $errors->first('lokasi') }}</small>@endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="uraian">Uraian</label>
                        <textarea class="form-control summernote-editor" id="uraian" rows="30" name="uraian">{{ old('uraian')}}</textarea>
                        @if ($errors->has('uraian'))
                        <small class="text-danger">{{ $errors->first('uraian') }}</small>@endif
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="{{ route('activity.index') }}"><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
</script>
@endpush
