<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Village;
use App\JobCompanion;
use App\Participant;
use App\ParticipantsCategory;

class SubDistrict extends Model
{
    protected $fillable =
    [
        'nama_kecamatan', 'profil', 'lat', 'long'
    ];
    protected $primarykey = 'id';
    public $timestamps = false;

    public function villages()
    {
        return $this->hasMany(Village::class, 'id_subdistrict', 'id');
    }

    public function districtToJob()
    {
        return $this->hasMany(JobCompanion::Class, 'id_district', 'id');
    }

    public function getParticipant()
    {
        return $this->hasMany(Participant::Class, 'id_district', 'id');
    }

    public function getParticipantKategori()
    {
        return $this->hasMany(ParticipantsCategory::Class, 'id_district', 'id');
    }
}
