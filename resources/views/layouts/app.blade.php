<?php
use App\Contact;

$contact = Contact::All()->first();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title') | {{ $contact->nama_lembaga }}</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('asset_admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('asset_admin/vendors/css/vendor.bundle.base.css') }}">
  <link rel="stylesheet" href="{{ asset('asset_admin/vendors/css/vendor.bundle.addons.css') }}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('asset_admin/css/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('asset_admin/images/favicon.png') }}" />
</head>

<body>
  @yield('content')
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{ asset('asset_admin/vendors/js/vendor.bundle.base.js') }}"></script>
  <script src="{{ asset('asset_admin/vendors/js/vendor.bundle.addons.js') }}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{ asset('asset_admin/js/off-canvas.js') }}"></script>
  <script src="{{ asset('asset_admin/js/misc.js') }}"></script>
  <!-- endinject -->
</body>

</html>