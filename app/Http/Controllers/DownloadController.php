<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Download;
use Webpatser\Uuid\Uuid;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $download = Download::All();
        return view('download.index', compact('download'));
        
    }

    public function showDownload()
    {
        $download = Download::All();
        return view('download.showDownload', compact('download'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('download.formCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'nullable|max:100',
            'file' => 'required|file|max:5000'
        ]);

        $uploadedFile = $request->file('file');
        $path = $uploadedFile->store('public/files');

        $file = Download::create([
            'title' => $request->title ?? $uploadedFile->getClientOriginalName(),
            'file' => $path
        ]);
        return redirect()->route('download.index')->with('success','File berhasil diupload !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $category = NewsCategory::find($id);
        // $news = $category->news()->paginate(10);

        // return view('news.showByCategory', compact(['category','news']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $download = Download::where('id', $id)->first();
        return view('download.formEdit', compact('download'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'url' => 'required'
        ]);

        $download = Download::find($id);
        $download->url = $request->get('url');

        try {
            $download->save();
            return redirect()->route('download.index')->with('success','URL Google Drive berhasil diupdate !');
        }
        catch (\Exception $e) { 
            return back()->with('error','URL Google Drive gagal diupdate !');
        }         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Download::destroy($id);
        return back()->with('success', 'URL Google Drive berhasil dihapus!');
    }
}
