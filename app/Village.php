<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SubDistrict;
use App\JobCompanion;
use App\Participant;

class Village extends Model
{
    protected $fillable = 
    [
        'id_subdistrict', 'nama_desa', 'jml_penerima_bantuan', 'keterangan'
    ];

    public function subdisctrict()
    {
        return $this->belongsTo(SubDistrict::class, 'id_subdistrict', 'id');
    }


    public function getParticipant()
    {
        return $this->hasMany(Participant::Class, 'id_village', 'id');
    }
}
