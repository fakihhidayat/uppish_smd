@extends('layouts.admin')

@section('title')
    Post Video Baru
@endsection

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="flash-message">
                    <p class="alert alert-success">{{ $message }}  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                </div>
                @endif
            </div>
        
            
        <div class="col-md-12 grid-margin stretch-card">
                
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">Video | Post Video Baru</h4>
                <form class="forms-sample" action="{{ route('video.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Video" value="{{ old('judul') }}">
                        @if ($errors->has('judul'))
                        <small class="text-danger">{{ $errors->first('judul') }}</small>@endif
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="kategori">Kategori</label>
                                {!! Form::select('kategori', $kategori, null, ['class'=>'form-control', 'placeholder'=> '-- Kategori Video --', 'value' => '{{ old("kategori")}}']) !!}
                                @if ($errors->has('kategori'))
                                <small class="text-danger">{{ $errors->first('kategori') }}</small>@endif
                            </div>
                        </div>
                        
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>URL Youtube</label>
                                <input type="text" class="form-control" id="url" name="url" placeholder="URL Video dari Youtube" value="{{ old('url') }}">
                                @if ($errors->has('url'))
                                <small class="text-danger">{{ $errors->first('url') }}</small>@endif
                                <br><label><i>Contoh</i></label><br>
                                <img src="{{ asset('asset_general/images/video/video.jpg') }}" id="example-url" width="400px" />
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                                <div class="form-group">
                                    <label>Preview</label><br>
                                    <div class="entry-thumbnail embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed" id="preview-video" frameborder="0" width allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                    </div>
                
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="{{ route('video.index') }}"><button class="btn btn-light">Cancel</button></a>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    
	$("#url").bind("keyup change", function(e) { 
        document.getElementById('preview-video').src = 'https://www.youtube.com/embed/'+document.getElementById('url').value;
    });
});


</script>
@endpush

