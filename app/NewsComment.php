<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\News;

class NewsComment extends Model
{
    protected $fillable = 
    [
        'id_berita', 'nama', 'email', 'komentar'
    ];

    public function getCreatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])->format('d, M Y');
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    public function news()
    {
        return $this->belongsTo(News::class, 'id_berita', 'id');
    }
}
