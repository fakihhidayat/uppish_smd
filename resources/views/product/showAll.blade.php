
@extends('layouts.frontend')

@section('title')
    Produk-Produk Ekonomi
@endsection

@section('content')
<div class="container">
    <br>
        <div class="section">
        <div class="row">
            <div class="col-md-8 col-lg-9">
                <div class="section world-news">
                        <h1 class="section-title">Informasi Kontak</h1>
                        <div class="stock-exchange text-center">
                            <div class="stock-exchange-zone">
                                <h2>Penjualan Dengan Sistem Pre-order</h2>
                            </div>
                            <div class="stock-reports">
                                <div class="com-details">
                                    <div class="row">
                                        <div class="col-3 com-name" align="right"><b>No. Telepon <i class="fa fa-phone-square"></i></b></div>
                                        <div class="col-3 current-price" align="left">{{ $contact->no_telp }}</div>
                                        <div class="col-3 current-price" align="right"><b>Whatsapp <i class="fab fa-whatsapp"></i></b></div>
                                        <div class="col-3 current-price" align="left">{{ $contact->wa }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3 com-name" align="right"><b>LINE <i class="fab fa-line"></i></b></div>
                                        <div class="col-3 current-price" align="left">{{ $contact->line }}</div>
                                        <div class="col-3 current-price" align="right"><b>E-mail <i class="fa fa-envelope"></i></b></div>
                                        <div class="col-3 current-price" align="left">{{ $contact->email }}</div>
                                    </div>
                                </div>													
                            </div>												
                        </div>
                    <br>
                    <h1 class="section-title">Produk - Produk</h1>
                    <div class="section">
                        <div class="row">
                            @if(count($products) > 0)
                            @foreach ($products as $product)
                            <div class="col-md-6 col-lg-4">
                                <div class="post medium-post">
                                    <div class="entry-header">
                                        <div class="entry-thumbnail">
                                            <a href="{{ asset('asset_general/images/product/'.$product->foto.'') }}" class="image-link"><img class="img-fluid" src="{{ asset('asset_general/images/product/'.$product->foto.'') }}" alt="" /></a>
                                        </div>
                                    </div>
                                    <div class="post-content">	
                                        <h3 align="center">{{ $product->nama_produk }}</h3>							
                                        <table class="table table-sm">
                                                <tbody>
                                                  <tr>
                                                    <th scope="row">Harga</th>
                                                    <td>Rp {{ $product->harga }}/{{ $product->jual_per }}</td>
                                                  </tr>
                                                  <tr>
                                                    <th scope="row">Deskripsi</th>
                                                    <td>{{ $product->deskripsi }}</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                              
                                    </div>
                                </div><!--/post--> 											
                            </div>   
                            @endforeach
                            @else
                            <div class="col-lg-12" style="margin-top: -3px">
                                <div class="left-content">
                                    <div class="details-news">											
                                        <div class="post">
                                            <div class="post-content">								
                                                <div class="entry-content" align="center">
                                                    <h3>-- Belum Ada Data --</h3>
                                                </div>
                                            </div>
                                        </div><!--/post--> 
                                    </div><!--/.section-->
                                </div><!--/.left-content-->
                            </div>
                            @endif
                        </div>
                    </div><!--/.section -->	
                </div><!--/.section-->

                <div class="pagination-wrapper">
                    @if ($products->hasPages())
                    <ul class="pagination">
                        @if ($products->onFirstPage())
                            <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                                <span aria-hidden="true">&lsaquo;</span>
                            </li>
                        @else
                            <li ><a href="{{ $products->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"><span aria-hidden="true"><i class="fa fa-long-arrow-left"></i> Previous Page</span></a></li>
                        @endif

                        @foreach ($products as $product)
                            @if (is_string($product))
                                <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                            @endif

                            @if (is_array($product))
                                @foreach ($product as $page => $url)
                                    @if ($page == $products->currentPage())
                                        <li class="active" aria-current="page"><span>{{ $page }}</span></li>
                                    @else
                                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                        
                        @if ($products->hasMorePages())
                            <li><a href="{{ $products->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"><span aria-hidden="true">Next Page <i class="fa fa-long-arrow-right"></i></span></a></li>
                        @else
                            <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                                <span aria-hidden="true">&rsaquo;</span>
                            </li>
                        @endif
                    </ul>
                    @endif
                </div>
            </div>
            @include('layouts.frontend_sidebar')
        </div>				
    </div><!--/.section-->
    </div><!--/.container-->
@endsection

